﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    public class Config
    {
        public static int SERVER_PORT = 7777;

        public enum Direction : int
        {
            NE = 0,
            NW = 1,
            SW = 2,
            SE = 3,
            N = 4,
            S = 5,
            E = 6,
            W = 7
        }
    }
}
