﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Util
{
    public class PathFinding
    {
        //a* path-calculation
        public static List<Node> astar(Position startPos, Position destinationPos, Area area)
        {
	        //create start and destination as true nodes
            Node start = new Node(startPos.X, startPos.Y, -1, -1, -1, -1);
            Node destination = new Node(destinationPos.X, destinationPos.Y, -1, -1, -1, -1);

	        //list of open nodes (nodes to be inspected)
	        List<Node> open = new List<Node>(); 
	
	        //list of closed nodes (nodes we've already inspected)
	        List<Node> closed = new List<Node>(); 

	        //cost from start to current node
	        var g = 0; 
	
	        //cost from current node to destination
	        var h = heuristic(start, destination); 
	
	        //cost from start to destination going through the current node
	        var f = g + h; 

	        //push the start node onto the list of open nodes
	        open.Add(start); 

	        //keep going while there's nodes in our open list
	        while (open.Count > 0)
	        {
		        //open list sorted lowest to highest (lowest f-value at index 0)
		        var bestCost = open[0].f;
		        var bestNode = 0;

		        for (var i = 1; i < open.Count; i++)
		        {
			        if (open[i].f < bestCost)
			        {
				        bestCost = open[i].f;
				        bestNode = i;
			        }
		        }

		        //set it as our current node
		        var currentNode = open[bestNode];

		        //check if we've reached our destination
		        if (currentNode.x == destination.x && currentNode.y == destination.y)
		        {
			        //initialize the path with the destination node
                    List<Node> path = new List<Node>();
                    path.Add(destination);

			        //go up the chain to recreate the path 
			        while (currentNode.parentIndex != -1)
			        {
				        currentNode = closed[currentNode.parentIndex];
				        path.Add(currentNode);
			        }
                    path.Reverse();
			        return path;
		        }

		        //remove the current node from our open list
		        open.Remove(open[bestNode]);

		        //push it onto the closed list
		        closed.Add(currentNode);

		        //expand our current node (look in all 8 directions)
                Node oldPos = new Node(currentNode.x, currentNode.y, 0, 0, 0, 0);

		        List<Node> newPos = new List<Node>();
		
		        newPos.Add(utilGetNewPos(Config.Direction.N, oldPos));
                newPos.Add(utilGetNewPos(Config.Direction.NE, oldPos));
                newPos.Add(utilGetNewPos(Config.Direction.E, oldPos));
                newPos.Add(utilGetNewPos(Config.Direction.SE, oldPos));
                newPos.Add(utilGetNewPos(Config.Direction.S, oldPos));
                newPos.Add(utilGetNewPos(Config.Direction.SW, oldPos));
                newPos.Add(utilGetNewPos(Config.Direction.W, oldPos));
                newPos.Add(utilGetNewPos(Config.Direction.NW, oldPos));
                newPos.Add(oldPos);
		
		        for (var i = 0; i < newPos.Count; i++)
		        {
			        //if the new node is open or the new node is our destination
			        if (area.GetTile(new Position(newPos[i].x, newPos[i].y)) != null &&
                        area.GetTile(new Position(newPos[i].x, newPos[i].y)).IsWalkable() || (destination.x == newPos[i].x && destination.y == newPos[i].y)) 
			        {
				        //see if the node is already in our closed list. if so, skip it.
				        var found_in_closed = false;
				        foreach (Node j in closed)
				        {
					        if (j.x == newPos[i].x && j.y == newPos[i].y)
					        {
						        found_in_closed = true;
						        break;
					        }
				        }

				        if (found_in_closed)
					        continue;

				        //see if the node is in our open list. If not, use it.
				        var found_in_open = false;
				        foreach (Node j in open)
				        {
					        if (j.x == newPos[i].x && j.y == newPos[i].y)
					        {
						        found_in_open = true;
						        break;
					        }
				        }

				        if (!found_in_open)
				        {
					        var newNode = new Node(newPos[i].x, newPos[i].y, closed.Count-1, -1, -1, -1);

					        newNode.g = currentNode.g + cost(currentNode, newNode);
					        newNode.h = heuristic(newNode, destination);
					        newNode.f = newNode.g + newNode.h;
                            area.GetTile(new Position(newNode.x, newNode.y)).f = f;

					        open.Add(newNode);
				        }
			        }
		        }
	        }

	        return new List<Node>();
        }

        public static int cost(Node currentNode, Node newNode)
        {
            int score = -1;
	
        	Config.Direction direction = utilGetDirection(currentNode, newNode);

            if (direction == Config.Direction.N || direction == Config.Direction.E || direction == Config.Direction.S || direction == Config.Direction.W)
		        score = 14;
	        else
		        score = 10;
		
	        return score;
        }

        public static Node utilGetNewPos(Config.Direction direction, Node pos)
        {
            bool oddrow = (pos.y % 2 != 0);
            Node delta = new Node(0, 0, 0, 0, 0, 0);
            switch (direction)
            {
                case Config.Direction.NE:
                    if (oddrow)
                    {
                        delta.x = 1;
                    }
                    delta.y = -1;
                    break;
                case Config.Direction.NW:
                    if (!oddrow)
                    {
                        delta.x = -1;
                    }
                    delta.y = -1;
                    break;
                case Config.Direction.SW:
                    if (!oddrow)
                    {
                        delta.x = -1;
                    }
                    delta.y = 1;
                    break;
                case Config.Direction.SE:
                    if (oddrow)
                    {
                        delta.x = 1;
                    }
                    delta.y = 1;
                    break;
                case Config.Direction.N:
                    delta.y = -2;
                    break;
                case Config.Direction.S:
                    delta.y = 2;
                    break;
                case Config.Direction.E:
                    delta.x = 1;
                    break;
                case Config.Direction.W:
                    delta.x = -1;
                    break;
                default:
                    break;
            }
            delta.x = pos.x + delta.x;
            delta.y = pos.y + delta.y;
            return delta;
        }

        public static Config.Direction utilGetDirection(Node n1, Node n2)
        {
            Position delta = new Position(n1.x - n2.x, n1.y - n2.y);

            if (delta.X == 0 && delta.Y == -2)
            {
                return Config.Direction.N;
            }
            else if (delta.X == 0 && delta.Y == 2)
            {
                return Config.Direction.S;
            }
            else if (delta.X == 1 && delta.Y == 0)
            {
                return Config.Direction.E;
            }
            else if (delta.X == -1 && delta.Y == 0)
            {
                return Config.Direction.W;
            }
            if (n1.y % 2 != 0)
            {
                if (delta.X == 1 && delta.Y == -1)
                {
                    return Config.Direction.NE;
                }
                else if (delta.X == 0 && delta.Y == -1)
                {
                    return Config.Direction.NW;
                }
                else if (delta.X == 0 && delta.Y == 1)
                {
                    return Config.Direction.SW;
                }
                else if (delta.X == 1 && delta.Y == 1)
                {
                    return Config.Direction.SE;
                }
            }
            else
            {
                if (delta.X == 0 && delta.Y == -1)
                {
                    return Config.Direction.NE;
                }
                else if (delta.X == -1 && delta.Y == -1)
                {
                    return Config.Direction.NW;
                }
                else if (delta.X == -1 && delta.Y == 1)
                {
                    return Config.Direction.SW;
                }
                else if (delta.X == 0 && delta.Y == 1)
                {
                    return Config.Direction.SE;
                }
            }
            return Config.Direction.N;
        }

        public static Node Delta(Config.Direction dir, bool oddrow)
        {
            Node delta = new Node(0, 0, 0, 0, 0, 0);
            switch (dir)
            {
                case Config.Direction.NE:
                    if (oddrow)
                    {
                        delta.x = 1;
                    }
                    delta.y = -1;
                    break;
                case Config.Direction.NW:
                    if (!oddrow)
                    {
                        delta.x = -1;
                    }
                    delta.y = -1;
                    break;
                case Config.Direction.SW:
                    if (!oddrow)
                    {
                        delta.x = -1;
                    }
                    delta.y = 1;
                    break;
                case Config.Direction.SE:
                    if (oddrow)
                    {
                        delta.x = 1;
                    }
                    delta.y = 1;
                    break;
                case Config.Direction.N:
                    delta.y = -2;
                    break;
                case Config.Direction.S:
                    delta.y = 2;
                    break;
                case Config.Direction.E:
                    delta.x = 1;
                    break;
                case Config.Direction.W:
                    delta.x = -1;
                    break;
                default:
                    break;
            }
            return delta;
        }

        public static double heuristic(Node currentNode, Node destination)
        {
            return Math.Floor(Math.Sqrt(Math.Pow(currentNode.x-destination.x, 2)+Math.Pow(currentNode.y-destination.y, 2)));
        }

        public class Node
        {
            public int x;
            public int y;
            public int parentIndex;
            public int g;
            public double h;
            public double f;

            public Node(int _x, int _y, int _parentIndex, int _g, double _h, double _f)
            {
                this.x = _x;
                this.y = _y;
                this.parentIndex = _parentIndex;
                this.g = _g;
                this.h = _h;
                this.f = _f;
            }
        }
    }
}
