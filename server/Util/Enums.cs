﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Util
{
    public class Enums
    {
        public enum ThingProp : byte
        {
            Direction = 0x00,
            Health = 0x01,
            Mana = 0x02,
            StatsSTR = 0x03,
            StatsCONS = 0x04,
            StatsAGI = 0x05,
            StatsDEX = 0x06,
            StatsINT = 0x07,
            StatsWIS = 0x08,
            SkillPoints = 0x09,
            StatsPoints = 0x10
        }

        public enum Stat : byte
        {
            STR = 0x00,
            CONS = 0x01,
            AGI = 0x02,
            DEX = 0x03,
            INT = 0x04,
            WIS = 0x05
        }

        public enum SkillType
        {
            Activable,
            Castable,
            Damage,
            Passive
        }

        public enum Effect : byte
        {
            Login = 1,
            Blood = 2,
            Health = 3
        }

        public enum AttributeType
        {
            None,
            AttackSpeed,
            AttackDamage
        }

        public enum ItemGroup : int
        {
            None = 0,
            Object,
            Container,
            Equipment,
            Weapon
        }

        public enum SlotType : int
        {
            None = 0,
            Right_Earing = 1,
            Head = 2,
            Left_Earing = 3,
            Right_Shoulder = 4,
            Neck = 5,
            Left_Shoulder = 6,
            Right_Hand = 7,
            Armor = 8,
            Left_Hand = 9,
            Leg = 10,
            Foot = 11,
            Container1 = 12,
            Container2 = 13,
            Container3 = 14,
            Container4 = 15,
            First = Right_Earing,
            Last = Container4
        }
    }
}
