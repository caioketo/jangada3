﻿using server.Content;
using server.Content.Infos;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using TiledSharp;

namespace server.Util
{
    public class Loader
    {
        public static void LoadAllMaps()
        {
            var ext = new List<string> {".tmx"};
            List<string> myFiles = Directory.GetFiles(Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().Location) + @"\Resources\Maps\", 
                "*.*", SearchOption.TopDirectoryOnly)
                 .Where(s => ext.Any(e => s.EndsWith(e))).ToList();
            foreach (string file in myFiles)
            {
                LoadMap(file);
            }
        }
        public static void LoadMap(string path)
        {
            TmxMap map = new TmxMap(path);
            int i = 0;
            int areaId;
            string filename = Path.GetFileNameWithoutExtension(path);
            try
            {
                areaId = Convert.ToInt32(filename);
            }
            catch (Exception)
            {
                Console.WriteLine("Nome de arquivo não permitido, adicionando área como Id=0");
                areaId = 0;
            }
            Area area = new Area(areaId, map.Width, map.Height);
            foreach (TmxLayerTile mapTile in map.Layers[i].Tiles)
            {
                if (mapTile.Gid > 0)
                {
                    Tile tile = new Tile();
                    tile.Ground = mapTile.Gid;
                    tile.Pos = new Position(mapTile.X, mapTile.Y);
                    area.AddTile(tile);
                }
            }
            i++;
            foreach (TmxLayerTile mapTile in map.Layers[i].Tiles)
            {
                if (mapTile.Gid > 0)
                {
                    Thing thing = new Thing();
                    thing.SpriteId = mapTile.Gid - (map.Tilesets[1].FirstGid - 1);
                    thing.Pos = new Position(mapTile.X, mapTile.Y);
                    area.AddThing(thing);
                }
            }
            Game.Instance.Areas.Add(area);
        }
    }
}
