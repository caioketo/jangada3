﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Util
{
    public class ModifyAttribute : IAttribute
    {
        private int percent;

        public ModifyAttribute(int percent)
        {
            this.percent = percent;
        }

        public int GetModifier(uint value)
        {
            return (int)(value * (percent / 100));
        }
    }
}
