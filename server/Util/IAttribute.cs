﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Util
{
    public interface IAttribute
    {
        int GetModifier(uint value);
    }
}
