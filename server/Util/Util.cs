﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Util
{
    public class Util
    {
        public static Dictionary<ushort, uint> ExpByLevel = new Dictionary<ushort, uint>();

        public static uint GetNextLevelExp(ushort level)
        {
            if (ExpByLevel.ContainsKey(level))
            {
                return ExpByLevel[level];
            }
            else
            {
                return 0;
            }
        }

        public static server.Util.Enums.AttributeType ParseAttributeType(string attributeType)
        {
            switch (attributeType)
            {
                case "AttackDamage":
                    return server.Util.Enums.AttributeType.AttackDamage;
                case "AttackSpeed":
                    return server.Util.Enums.AttributeType.AttackSpeed;
                default:
                    return server.Util.Enums.AttributeType  .None;
            }
        }
    }
}
