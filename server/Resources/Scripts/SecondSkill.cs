﻿using server.Content;
using server.Content.Infos;
using server.Scripting;
using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Resources.Scripts
{
    public class SecondSkill : ISkill
    {
        Skill skill;
        Player caster;

        public void onInit(Skill skill, Player caster)
        {
            //onInit = Passives uses it
            this.skill = skill;
            this.caster = caster;
            caster.OnGetASHandler += AddASPassive;
        }

        public void onCast()
        {
            //onCast = Castables
            caster.OnGetASHandler += AddAS;
            Scheduler.AddTask(this.RemoveStat, null, (int)5000);
        }

        public uint AddAS(uint baseAS)
        {
            return (uint)(baseAS * 0.2f);
        }

        public uint AddASPassive(uint baseAS)
        {
            return (uint)(baseAS * 0.05f);
        }

        private void RemoveStat()
        {
            caster.OnGetASHandler -= AddAS;
        }

        public void onActivate()
        {
            //onActivate = Activables
        }

        public void onDesactivate()
        {
            //onDesactivate = Activables 
        }

        public void onEnd()
        {
            //onEnd
        }

        public SkillInfo getInfo()
        {
            SkillInfo info = new SkillInfo();

            info.Name = "Second Skill";
            info.Description = "Second Skill";
            info.Id = 2;
            info.LevelRequired = 0;
            info.Cooldown = 10;
            info.Type = server.Util.Enums.SkillType.Damage;
            info.Class = 1;
            info.PositionInTree = 1;
            info.Radius = 2;
            info.Targetable = true;
            info.effect = Enums.Effect.Blood;
            return info;
        }


        public int getDamage()
        {
            return 50;
        }

        public bool hasPassive()
        {
            return true;
        }
    }
}
