﻿using server.Content;
using server.Content.Infos;
using server.Scripting;
using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Resources.Scripts
{
    public class FirstSkill : ISkill
    {
        Skill skill;
        Player caster;

        public void onInit(Skill skill, Player caster)
        {
            //onInit = Passives uses it
            this.skill = skill;
            this.caster = caster;
            caster.OnGetASHandler += AddASPassive;
        }

        public void onCast()
        {
            //onCast = Castables
            caster.OnGetASHandler += AddAS;
            Scheduler.AddTask(this.RemoveStat, null, (int)5000);
        }

        public uint AddAS(uint baseAS)
        {
            return (uint)(baseAS * 0.2f);
        }

        public uint AddASPassive(uint baseAS)
        {
            return (uint)(baseAS * 0.05f);
        }

        private void RemoveStat()
        {
            caster.OnGetASHandler -= AddAS;
        }

        public void onActivate()
        {
            //onActivate = Activables
        }

        public void onDesactivate()
        {
            //onDesactivate = Activables 
        }

        public void onEnd()
        {
            //onEnd
        }

        public SkillInfo getInfo()
        {
            SkillInfo info = new SkillInfo();

            info.Name = "First Skill";
            info.Description = "First Skill";
            info.Id = 1;
            info.LevelRequired = 0;
            info.Cooldown = 10;
            info.Type = server.Util.Enums.SkillType.Castable;
            info.Class = 1;
            info.PositionInTree = 0;
            return info;
        }


        public int getDamage()
        {
            return 0;
        }

        public bool hasPassive()
        {
            return true;
        }
    }
}
