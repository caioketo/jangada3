﻿using Microsoft.CSharp;
using server.Content.Infos;
using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace server.Scripting
{
    public class ScriptManager
    {
        private static CSharpCodeProvider cSharpCodeProvider = new CSharpCodeProvider(new Dictionary<string, string>() { { "CompilerVersion", "v4.0" } });
        //private static List<IScript> loadedScripts = new List<IScript>();
        private static StringBuilder errorLog;


        public static string LoadAllScripts()
        {
            errorLog = new StringBuilder();
            string scriptDir = @"Resources\Scripts\";
            if (!Directory.Exists(scriptDir)) return "Directory not found";
            foreach (string path in Directory.GetFiles(scriptDir))
            {
                // TODO: concatenate files, then load
                if (!File.Exists(path)) continue;
                LoadScript(path);
            }
            return errorLog.ToString();
        }

        public static void LoadScript(string path)
        {
            Assembly assembly = null;
            switch (Path.GetExtension(path))
            {
                case ".cs":
                    assembly = CompileScript(path, cSharpCodeProvider);
                    break;
            }

            if (assembly != null)
            {
                foreach (ISkill skill in FindInterfaces<ISkill>(assembly))
                {
                    SkillInfo info = skill.getInfo();
                    info.Script = skill;
                    SkillInfo.AddInfo(info.Id, info);
                }
            }
        }

        public static Assembly CompileScript(string path, CodeDomProvider provider)
        {
            CompilerParameters compilerParameters = new CompilerParameters();
            compilerParameters.GenerateExecutable = false;
            compilerParameters.GenerateInMemory = true;
            compilerParameters.IncludeDebugInformation = false;
            compilerParameters.ReferencedAssemblies.Add("System.dll");
            compilerParameters.ReferencedAssemblies.Add("System.Core.dll");
            compilerParameters.ReferencedAssemblies.Add("System.Data.Linq.dll");
            compilerParameters.ReferencedAssemblies.Add(System.Reflection.Assembly.GetExecutingAssembly().Location);
            CompilerResults results = provider.CompileAssemblyFromFile(compilerParameters, path);
            if (!results.Errors.HasErrors)
            {
                return results.CompiledAssembly;
            }
            else
            {
                foreach (CompilerError error in results.Errors)
                    errorLog.AppendLine(error.ToString());
            }
            return null;
        }

        public static IEnumerable<IType> FindInterfaces<IType>(Assembly assembly)
        {
            foreach (Type t in assembly.GetTypes())
            {
                if (t.GetInterface(typeof(IType).Name, true) != null && !t.IsInterface && !t.IsAbstract)
                {
                    yield return (IType)assembly.CreateInstance(t.FullName);
                }
            }
        }
    }
}
