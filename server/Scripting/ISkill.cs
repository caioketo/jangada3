﻿using server.Content;
using server.Content.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Scripting
{
    public interface ISkill
    {
        void onInit(Skill skill, Player caster);
        void onCast();
        void onActivate();
        void onDesactivate();
        void onEnd();
        int getDamage();
        SkillInfo getInfo();
        bool hasPassive();
    }
}
