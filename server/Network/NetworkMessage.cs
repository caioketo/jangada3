﻿using server.Content;
using server.Content.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network
{
    public class NetworkMessage
    {
        #region Static Methods

        public static NetworkMessage Concat(NetworkMessage message1, NetworkMessage message2)
        {
            NetworkMessage concat = new NetworkMessage();
            Array.Copy(message1.buffer, 6, concat.buffer, 0, message1.length - 6);
            Array.Copy(message2.buffer, 6, concat.buffer, message1.length, message2.length - 6);
            concat.length = message1.length + message2.length - 6;
            return concat;
        }

        #endregion

        #region Instance Variables

        private byte[] buffer;
        private int position, length, bufferSize = 1105920;

        #endregion

        #region Properties

        public int Length
        {
            get { return length; }
            set { length = value; }
        }

        public int Position
        {
            get { return position; }
            set { position = value; }
        }

        public byte[] Buffer
        {
            get { return buffer; }
            set { buffer = value; }
        }

        public int BufferSize
        {
            get { return bufferSize; }
        }

        public byte[] GetPacket()
        {
            byte[] t = new byte[length - 8];
            Array.Copy(buffer, 8, t, 0, length - 8);
            return t;
        }

        public byte[] ShortBuffer
        {
            get
            {
                byte[] t = new byte[length];
                Array.Copy(buffer, 0, t, 0, length);
                return t;
            }
        }

        #endregion

        #region Constructors

        public NetworkMessage()
        {
            Reset();
        }

        public NetworkMessage(int startingIndex)
        {
            Reset(startingIndex);
        }

        public void Reset(int startingIndex)
        {
            buffer = new byte[bufferSize];
            length = startingIndex;
            position = startingIndex;
        }

        public void Reset()
        {
            Reset(4);
        }

        #endregion

        #region Get

        public byte GetByte()
        {
            if (position + 1 > length)
                throw new IndexOutOfRangeException("NetworkMessage GetByte() out of range.");

            return buffer[position++];
        }

        public byte[] GetBytes(int count)
        {
            if (position + count > length)
                throw new IndexOutOfRangeException("NetworkMessage GetBytes() out of range.");

            byte[] t = new byte[count];
            Array.Copy(buffer, position, t, 0, count);
            position += count;
            return t;
        }

        public string GetString()
        {
            int len = (int)GetUInt16();
            string t = System.Text.ASCIIEncoding.Default.GetString(buffer, position, len);
            position += len;
            return t;
        }

        public ushort GetUInt16()
        {
            return BitConverter.ToUInt16(GetBytes(2), 0);
        }

        public uint GetUInt32()
        {
            return BitConverter.ToUInt32(GetBytes(4), 0);
        }

        public Guid GetGuid()
        {
            return new Guid(GetBytes(16));
        }

        private ushort GetPacketHeader()
        {
            return BitConverter.ToUInt16(buffer, 0);
        }

        public double GetDouble()
        {
            return (GetUInt32() / 10);
        }

        public Position GetPosition()
        {
            return new Position((int)GetUInt32(), (int)GetUInt32());
        }

        #endregion

        #region Add

        public void AddByte(byte value)
        {
            if (1 + length > bufferSize)
                throw new Exception("NetworkMessage buffer is full.");

            AddBytes(new byte[] { value });
        }

        public void AddBytes(byte[] value)
        {
            if (value.Length + length > bufferSize)
                throw new Exception("NetworkMessage buffer is full.");

            Array.Copy(value, 0, buffer, position, value.Length);
            position += value.Length;

            if (position > length)
                length = position;
        }

        public void AddString(string value)
        {
            AddUInt16((ushort)value.Length);
            AddBytes(System.Text.ASCIIEncoding.Default.GetBytes(value));
        }

        public void AddUInt16(ushort value)
        {
            AddBytes(BitConverter.GetBytes(value));
        }

        public void AddUInt32(uint value)
        {
            AddBytes(BitConverter.GetBytes(value));
        }

        public void AddGuid(Guid value)
        {
            AddBytes(value.ToByteArray());
        }

        public void AddPaddingBytes(int count)
        {
            position += count;

            if (position > length)
                length = position;
        }


        public void AddDouble(double value)
        {
            uint iValue = (uint)(value * 10);
            AddUInt32(iValue);
        }

        public void AddPosition(Position pos)
        {
            AddUInt32((uint)pos.X);
            AddUInt32((uint)pos.Y);
        }

        public void AddPlayerInfo(Player player)
        {
            AddGuid(player.GUID);
            AddUInt16((ushort)player.SpriteId);
            AddPosition(player.Pos);
            AddString(player.Name);
            AddUInt16((ushort)player.GetStat(Util.Enums.Stat.AGI));
            AddUInt16((ushort)player.GetStat(Util.Enums.Stat.CONS));
            AddUInt16((ushort)player.GetStat(Util.Enums.Stat.DEX));
            AddUInt16((ushort)player.GetStat(Util.Enums.Stat.INT));
            AddUInt16((ushort)player.GetStat(Util.Enums.Stat.STR));
            AddUInt16((ushort)player.GetStat(Util.Enums.Stat.WIS));
            AddUInt32((uint)player.Level);
            AddUInt32((uint)player.Experience);
            AddUInt32((uint)player.SkillPoints);
            AddUInt32((uint)player.StatPoints);
            AddUInt32((uint)player.Class.Id);
            AddUInt32((uint)player.Race.Id);
        }

        public void AddSkillInfo(SkillInfo info)
        {
            AddUInt32((uint)info.Id);
            AddUInt32(info.LevelRequired);
            AddUInt16(info.PositionInTree);
            AddUInt16((ushort)info.RequiredSkills.Count);
            if (info.RequiredSkills.Count > 0)
            {
                var reqSkills = info.RequiredSkills.Keys.ToArray();
                foreach (ushort reqId in reqSkills)
                {
                    AddUInt16(reqId);
                    AddUInt32(info.RequiredSkills[reqId]);
                }
            }
        }

        public void AddSkill(Skill skill)
        {
            AddUInt32((uint)skill.Id);
            AddUInt32((uint)skill.Level);
            AddUInt32(skill.Info.Cooldown);
            if (skill.Info.Targetable)
            {
                AddByte(1);
                AddDouble(skill.Info.Radius);
            }
            else
            {
                AddByte(0);
            }
        }

        #endregion

        #region Peek

        public byte PeekByte()
        {
            return buffer[position];
        }

        public byte[] PeekBytes(int count)
        {
            byte[] t = new byte[count];
            Array.Copy(buffer, position, t, 0, count);
            return t;
        }

        public ushort PeekUInt16()
        {
            return BitConverter.ToUInt16(PeekBytes(2), 0);
        }

        public uint PeekUInt32()
        {
            return BitConverter.ToUInt32(PeekBytes(4), 0);
        }

        public string PeekString()
        {
            int len = (int)PeekUInt16();
            return System.Text.ASCIIEncoding.ASCII.GetString(PeekBytes(len + 2), 2, len);
        }

        #endregion

        #region Replace

        public void ReplaceBytes(int index, byte[] value)
        {
            if (length - index >= value.Length)
                Array.Copy(value, 0, buffer, index, value.Length);
        }

        #endregion

        #region Skip

        public void SkipBytes(int count)
        {
            if (position + count > length)
                throw new IndexOutOfRangeException("NetworkMessage SkipBytes() out of range.");
            position += count;
        }

        #endregion

        #region Prepare

        private void InsertIdentifier()
        {
            byte[] id = { 0x01, 0x02 };
            Array.Copy(id, 0, buffer, 0, 2);
        }

        private void InsertTotalLength()
        {
            try
            {
                byte[] len = new byte[4];
                len = BitConverter.GetBytes((int)(length - 4));
                Array.Copy(len, 0, buffer, 0, 4);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool PrepareToSendWithoutEncryption()
        {
            //InsertIdentifier();
            InsertTotalLength();

            return true;
        }

        public bool PrepareToSend()
        {
            //InsertIdentifier();
            InsertTotalLength();

            return true;
        }

        public bool PrepareToRead()
        {
            position = 4;
            return true;
        }

        #endregion
    }
}
