﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace server.Network
{
    public delegate void ParsePacket(byte type, NetworkMessage message, Connection connection);

    public class Server
    {
        private TcpListener listener;
        public List<Connection> connections = new List<Connection>();
        private ParsePacket parse;

        public Server(ParsePacket _parse)
        {
            listener = new TcpListener(IPAddress.Any, Config.SERVER_PORT);
            parse = _parse;
        }

        public void Run()
        {
            listener.Start();
            listener.BeginAcceptSocket(new AsyncCallback(ListenerCallback), listener);
        }

        private void ListenerCallback(IAsyncResult ar)
        {
            Connection connection = new Connection(parse);
            connection.ListenerCallback(ar);
            connections.Add(connection);
            listener.BeginAcceptSocket(new AsyncCallback(ListenerCallback), listener);
        }
    }
}
