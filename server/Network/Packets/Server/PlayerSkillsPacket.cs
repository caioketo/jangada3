﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class PlayerSkillsPacket
    {
        public static void Add(NetworkMessage message, Player player)
        {
            if (player.SkillsLearnt.Count > 0)
            {
                message.AddByte((byte)MessageType.PlayerSkillsPacket);
                message.AddUInt32((uint)player.SkillsLearnt.Count);
                foreach (Skill skill in player.SkillsLearnt)
                {
                    message.AddSkill(skill);
                }
            }
        }
    }
}
