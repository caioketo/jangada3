﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class NewSkillPacket
    {
        public static void Add(NetworkMessage message, Skill skill)
        {
            message.AddByte((byte)MessageType.NewSkill);
            message.AddSkill(skill);
        }
    }
}
