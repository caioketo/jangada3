﻿using server.Content;
using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class EffectPacket
    {
        public static void Add(NetworkMessage message, Position pos, Enums.Effect effect)
        {
            message.AddByte((byte)MessageType.Effect);
            message.AddByte((byte)effect);
            message.AddPosition(pos);
        }
    }
}
