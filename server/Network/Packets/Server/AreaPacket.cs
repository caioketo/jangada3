﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class AreaPacket
    {
        public static void AddAreaDescription(NetworkMessage message, Area area)
        {
            message.AddUInt32((uint)area.Width);
            message.AddUInt32((uint)area.Height);
            for (int y = 0; y <= (area.Height); y++)
            {
                for (int x = 0; x <= (area.Width); x++)
                {
                    Tile tile = area.GetTile(new Position(x, y));
                    AddTileDescription(message, tile);
                }
            }
        }

        public static void AddTileDescription(NetworkMessage message, Tile tile)
        {
            if (tile != null)
            {
                message.AddUInt16((ushort)tile.Ground);
            }
            else
            {
                message.AddUInt16(0);
            }
        }

        public static void AddThingsDescription(NetworkMessage message, Area area)
        {
            message.AddByte((byte)MessageType.ThingsDescriptionPacket);
            message.AddUInt32((uint)area.GetThings().Count);
            foreach (Thing thing in area.GetThings())
            {
                message.AddGuid(thing.GUID);
                message.AddUInt16((ushort)thing.SpriteId);
                message.AddPosition(thing.Pos);
            }
        }

        public static void AddCreaturesDescription(NetworkMessage message, Area area, Player player)
        {
            message.AddByte((byte)MessageType.CreaturesDescriptionPacket);
            message.AddUInt32((uint)(area.GetCreatures().Count + area.GetPlayerExcept(player).Count));
            foreach (Creature creature in area.GetCreatures())
            {
                message.AddGuid(creature.GUID);
                message.AddUInt16((ushort)creature.SpriteId);
                message.AddPosition(creature.Pos);
                message.AddString(creature.Name);
                message.AddUInt16((ushort)creature.GetStat(Util.Enums.Stat.AGI));
                message.AddUInt16((ushort)creature.GetStat(Util.Enums.Stat.CONS));
                message.AddUInt16((ushort)creature.GetStat(Util.Enums.Stat.DEX));
                message.AddUInt16((ushort)creature.GetStat(Util.Enums.Stat.INT));
                message.AddUInt16((ushort)creature.GetStat(Util.Enums.Stat.STR));
                message.AddUInt16((ushort)creature.GetStat(Util.Enums.Stat.WIS));
            }
            foreach (Player playerIn in area.GetPlayerExcept(player))
            {
                message.AddGuid(playerIn.GUID);
                message.AddUInt16((ushort)playerIn.SpriteId);
                message.AddPosition(playerIn.Pos);
                message.AddString(playerIn.Name);
                message.AddUInt16((ushort)playerIn.GetStat(Util.Enums.Stat.AGI));
                message.AddUInt16((ushort)playerIn.GetStat(Util.Enums.Stat.CONS));
                message.AddUInt16((ushort)playerIn.GetStat(Util.Enums.Stat.DEX));
                message.AddUInt16((ushort)playerIn.GetStat(Util.Enums.Stat.INT));
                message.AddUInt16((ushort)playerIn.GetStat(Util.Enums.Stat.STR));
                message.AddUInt16((ushort)playerIn.GetStat(Util.Enums.Stat.WIS));
            }
        }
    }
}
