﻿using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class UpdateCreaturePacket
    {
        public static void Add(NetworkMessage message, Guid thingGUID, Enums.ThingProp prop, object value)
        {
            List<Util.Enums.ThingProp> props = new List<Util.Enums.ThingProp>();
            props.Add(prop);
            List<object> values = new List<object>();
            values.Add(value);
            Add(message, thingGUID, props, values);
        }

        public static void Add(NetworkMessage message, Guid thingGUID, List<Enums.ThingProp> props, List<object> values)
        {
            message.AddByte((byte)MessageType.UpdateThingPacket);
            message.AddGuid(thingGUID);
            message.AddUInt16((ushort)props.Count);
            int i = 0;
            foreach (Enums.ThingProp prop in props)
            {
                byte propB = (byte)prop;
                message.AddByte(propB);
                switch (prop)
                {
                    case Enums.ThingProp.Direction:
                        message.AddUInt16((ushort)((Config.Direction)values[i]));
                        break;
                    case Enums.ThingProp.Health:
                    case Enums.ThingProp.Mana:
                    case Enums.ThingProp.StatsAGI:
                    case Enums.ThingProp.StatsCONS:
                    case Enums.ThingProp.StatsDEX:
                    case Enums.ThingProp.StatsINT:
                    case Enums.ThingProp.StatsSTR:
                    case Enums.ThingProp.StatsWIS:
                    case Enums.ThingProp.SkillPoints:
                        message.AddUInt32((uint)((int)values[i]));
                        break;
                    default:
                        break;
                }
                
                i++;
            }
        }
    }
}
