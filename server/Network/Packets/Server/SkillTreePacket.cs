﻿using server.Content;
using server.Content.Infos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class SkillTreePacket
    {
        public static void Add(NetworkMessage message, Player player)
        {
            message.AddByte((byte)MessageType.SkillTree);
            message.AddUInt32((uint)player.Class.skillTree.Skills.Count);

            foreach (ushort skillId in player.Class.skillTree.Skills)
            {
                SkillInfo sInfo = SkillInfo.GetSkillInfo(skillId);
                message.AddSkillInfo(sInfo);
            }
        }
    }
}
