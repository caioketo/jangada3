﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class PlayerLoginPacket
    {
        public static void Add(NetworkMessage message, Player player)
        {
            message.AddByte((byte)MessageType.PlayerLoginPacket);
            message.AddPlayerInfo(player);
        }
    }
}
