﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace server.Network.Packets.Server
{
    public class AreaDescriptionPacket : AreaPacket
    {
        public static void Add(NetworkMessage message, Player player)
        {
            message.AddByte((byte)MessageType.AreaDescriptionPacket);
            message.AddPosition(player.Pos);
            AddAreaDescription(message, player.Area);
            AddThingsDescription(message, player.Area);
            AddCreaturesDescription(message, player.Area, player);
        }
    }
}
