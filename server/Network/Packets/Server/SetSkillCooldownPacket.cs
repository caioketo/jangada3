﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class SetSkillCooldownPacket
    {
        public static void Add(NetworkMessage message, Skill skill)
        {
            message.AddByte((byte)MessageType.SetSkillCooldown);
            message.AddUInt32((uint)skill.Id);
        }
    }
}
