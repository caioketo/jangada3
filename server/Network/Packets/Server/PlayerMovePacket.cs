﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Network.Packets.Server
{
    public class PlayerMovePacket
    {
        public static void Add(NetworkMessage message, Player player)
        {
            message.AddByte((byte)MessageType.PlayerMovePacket);
            message.AddGuid(player.GUID);
            message.AddPosition(player.Pos);
        }
    }
}
