﻿using server.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace server.Network
{
    public class Connection
    {
        protected Socket socket;
        protected NetworkStream stream;
        protected NetworkMessage inMessage = new NetworkMessage();
        protected bool remove = false;
        protected List<NetworkMessage> buffer = new List<NetworkMessage>();
        protected ParsePacket parseFunction;
        public Player player;
        public bool InitialPacketSent = false;
        private bool IsSending = false;

        


        public Connection(ParsePacket parse) {
            parseFunction = parse;
        }

        public void ListenerCallback(IAsyncResult ar)
        {
            TcpListener clientListener = (TcpListener)ar.AsyncState;
            socket = clientListener.EndAcceptSocket(ar);
            stream = new NetworkStream(socket);

            stream.BeginRead(inMessage.Buffer, 0, 4, new AsyncCallback(ClientReadCallback), null);
        }

        public void ClientReadCallback(IAsyncResult ar)
        {
            if (!EndRead(ar))
            {
                return;
            }


            byte type = inMessage.GetByte();

            //Parse
            if (parseFunction != null)
            {
                parseFunction(type, inMessage, this);
            }

            if (!remove)
            {
                stream.BeginRead(inMessage.Buffer, 0, 4, new AsyncCallback(ClientReadCallback), null);
            }

        }


        private bool EndRead(IAsyncResult ar)
        {
            try
            {
                int read = stream.EndRead(ar);

                if (read == 0)
                {
                    // client disconnected
                    return false;
                }

                int size = (int)BitConverter.ToUInt32(inMessage.Buffer, 0) + 4;

                while (read < size)
                {
                    if (stream.CanRead)
                        read += stream.Read(inMessage.Buffer, read, size - read);
                }
                inMessage.Length = size;

                inMessage.Position = 0;

                inMessage.GetUInt32(); //total length

                return true;
            }
            catch (Exception ex)
            {
                //client disconnected
                return false;
            }
        }

        public void Send(NetworkMessage message)
        {
            Send(message, false);
        }

        public void Send(NetworkMessage message, bool force)
        {
            //if ((!canSendFunction(this)) && !force)
            //{
                //buffer.Add(message);
                //return;
            //}
            if (!force && (!InitialPacketSent || IsSending))
            {
                buffer.Add(message);
                return;
            }
            IsSending = true;
            if (!force && buffer.Count > 0)
            {
                NetworkMessage temp = message;
                foreach (NetworkMessage bufferMessage in buffer)
                {
                    temp = NetworkMessage.Concat(temp, bufferMessage);
                }
                message = temp;
            }
            message.PrepareToSend();
            try
            {
                string log = "";
                for (var i = 0; i < message.Length; i++)
                {
                    log += message.Buffer[i] + " ";
                }
                stream.BeginWrite(message.Buffer, 0, message.Length, null, null);
                buffer.Clear();
            }
            catch (Exception ex)
            {
                // disconnected
            }
            IsSending = false;
        }

        public void Close()
        {
            remove = true;
            stream.Close();
            socket.Close();
        }
    }
}
