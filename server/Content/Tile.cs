﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content
{
    public class Tile
    {
        public Position Pos { get; set; }
        public int Ground { get; set; }


        public Tile()
        {
            Pos = new Position();
            Ground = 0;
        }

        internal bool IsWalkable()
        {
            return true;
        }

        public double f { get; set; }
    }
}
