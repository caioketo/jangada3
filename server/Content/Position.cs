﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content
{
    public class Position
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Position(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Position()
        {
            X = 0;
            Y = 0;
        }

        public override bool Equals(object obj)
        {
            Position pos = obj as Position;

            return (pos.X == this.X && pos.Y == this.Y);
        }

        public override int GetHashCode()
        {
            return this.X.GetHashCode() + this.Y.GetHashCode();
        }

        internal bool InRange(Position p1, int range)
        {
            return InRange(p1, this, range);
        }

        internal static bool InRange(Position p1, Position p2, int range)
        {
            return Distance(p1, p2) <= range;
        }

        internal int Distance(Position p2)
        {
            return Distance(this, p2);
        }

        internal static int Distance(Position p1, Position p2)
        {
            bool NSW = ((p2.X < p1.X) || (p2.X == p1.X && p2.Y % 2 == 0 && p1.Y % 2 != 0));
            double difY = Math.Abs(p1.Y - p2.Y);
            double difX = Math.Abs(p1.X - p2.X);
            double dist = (difY / 2) + difX;
            if (NSW)
            {
                if (p1.Y % 2 == 0)
                {
                    return (int)Math.Floor(dist);
                }
                else
                {

                    return (int)Math.Ceiling(dist);
                }
            }
            else
            {
                if (p1.Y % 2 == 0)
                {
                    return (int)Math.Ceiling(dist);
                }
                else
                {
                    return (int)Math.Floor(dist);
                }
            }
        }
    }
}
