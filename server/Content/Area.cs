﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace server.Content
{
    public class Area
    {
        public int Id { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        private Dictionary<Position, Tile> tiles = new Dictionary<Position,Tile>();
        private Dictionary<Position, Thing> things = new Dictionary<Position, Thing>();
        private List<Creature> creatures = new List<Creature>();
        private List<Player> players = new List<Player>();
        
        public Area(int id, int width, int height)
        {
            Id = id;
            Width = width;
            Height = height;


            //remover
            Thing thing = new Thing();
            thing.SpriteId = 1;
            thing.Pos = new Position(11, 11);
            things.Add(thing.Pos, thing);
            Thing thing2 = new Thing();
            thing2.SpriteId = 1;
            thing2.Pos = new Position(11, 12);
            things.Add(thing2.Pos, thing2);
            Thing thing3 = new Thing();
            thing3.SpriteId = 1;
            thing3.Pos = new Position(10, 13);
            things.Add(thing3.Pos, thing3);

            Creature c = new Creature();
            c.Pos = new Position(10, 10);
            AddCreature(c);
        }

        public Area(int id) : this(id, 100, 100)
        {

        }

        public void Tick(long gameTick)
        {
            foreach (Creature creature in creatures)
            {
                new Thread(() =>
                    creature.DoBattle(gameTick)
                ).Start();
            }
            foreach (Player player in players)
            {
                new Thread(() =>
                    player.DoBattle(gameTick)
                ).Start();
            }
        }

        public Tile GetTile(Position pos)
        {
            Tile t;
            tiles.TryGetValue(pos, out t);
            return t;
        }

        public List<Thing> GetThings()
        {
            return things.Values.ToList();
        }

        public List<Thing> GetThingsInArea(Position startPos, Position endPos)
        {
            return things.Values.Where(t => (t.Pos.X >= startPos.X && t.Pos.X <= endPos.X &&
                t.Pos.Y >= startPos.Y && t.Pos.Y <= endPos.Y)).ToList();
        }

        public List<Creature> GetCreatures()
        {
            return creatures;
        }

        public List<Creature> GetCreatureInArea(Position startPos, Position endPos)
        {
            return creatures.Where(t => (t.Pos.X >= startPos.X && t.Pos.X <= endPos.X &&
                t.Pos.Y >= startPos.Y && t.Pos.Y <= endPos.Y)).ToList();
        }

        public Thing GetThingByGUID(Guid GUID)
        {
            return things.Where(t => t.Value.GUID.Equals(GUID)).FirstOrDefault().Value;
        }

        public Creature GetCreatureByGUID(Guid GUID)
        {
            return creatures.Where(c => c.GUID.Equals(GUID)).FirstOrDefault();
        }

        public void AddThing(Thing thing)
        {
            if (!things.ContainsValue(thing))
            {
                if (thing.GUID.Equals(Guid.Empty))
                {
                    thing.GUID = Guid.NewGuid();
                }
                things.Add(thing.Pos, thing);
            }
        }

        public void AddCreature(Creature creature)
        {
            if (!creatures.Contains(creature))
            {
                if (creature.GUID.Equals(Guid.Empty))
                {
                    creature.GUID = Guid.NewGuid();
                }
                creatures.Add(creature);
                creature.Area = this;
            }
        }

        public void AddPlayer(Player player)
        {
            if (!players.Contains(player))
            {
                players.Add(player);
            }
        }

        public List<Player> GetPlayers()
        {
            return players;
        }

        public List<Player> GetPlayerExcept(Player player)
        {
            return players.Where(p => !p.GUID.Equals(player.GUID)).ToList();
        }

        internal void AddTile(Tile tile)
        {
            if (!tiles.ContainsKey(tile.Pos))
            {
                tiles.Add(tile.Pos, tile);
            }
        }

        internal Creature GetCreatureInPos(Position pos)
        {
            return creatures.Where(c => c.Pos.Equals(pos)).FirstOrDefault();
        }

        internal IEnumerable<Position> GetTilesInRange(Position p1, double radius)
        {
            return tiles.Keys.Where(p2 => p1.Distance(p2) <= radius);
        }
    }
}
