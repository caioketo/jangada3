﻿using server.Scripting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content.Infos
{
    public class SkillInfo
    {
        public int Id;
        public string Name;
        public string Description;
        public server.Util.Enums.SkillType Type;
        public uint LevelRequired;
        public Dictionary<ushort, uint> RequiredSkills = new Dictionary<ushort, uint>();
        public uint Cooldown;
        public ISkill Script;
        public ushort Class;
        public ushort PositionInTree;
        public server.Util.Enums.Effect effect;
        public double Radius = 0;
        public bool Targetable = false;

        private static Dictionary<int, SkillInfo> skillInfoDictionary = new Dictionary<int, SkillInfo>();

        public static Dictionary<int, SkillInfo> GetDictionary()
        {
            return skillInfoDictionary;
        }

        public static SkillInfo GetSkillInfo(int Id)
        {
            return skillInfoDictionary[Id];
        }

        public static void SetDictionary(Dictionary<int, SkillInfo> dic)
        {
            skillInfoDictionary = dic;
        }

        public static void AddInfo(int id, SkillInfo info)
        {
            info.Id = id;
            skillInfoDictionary.Add(id, info);
        }
    }
}
