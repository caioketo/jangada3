﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content.Infos
{
    public class LootInfo
    {
        public List<int> Ids = new List<int>();
        public List<int> Chances = new List<int>();

        public List<Item> GetDrop()
        {
            List<Item> drop = new List<Item>();

            for (int i = 0; i < Ids.Count; i++)
            {
                Random rand = new Random();
                double r = rand.NextDouble();
                if ((r * 100) <= Chances[i])
                {
                    drop.Add(Item.Create(Ids[i]));
                }
            }


            return drop;
        }
    }
}
