﻿using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content.Infos
{
    public class CreatureInfo
    {
        public Dictionary<Enums.Stat, int> Stats = new Dictionary<Enums.Stat, int>();
        public string Name;
        public int BodyId;
        public LootInfo loot = new LootInfo();
        public int Experience;

        private static Dictionary<int, CreatureInfo> creatureInfoDictionary = new Dictionary<int, CreatureInfo>();

        public static CreatureInfo GetCreatureInfo(int id)
        {
            return creatureInfoDictionary[id];
        }

        public static Dictionary<int, CreatureInfo> GetDictionary()
        {
            return creatureInfoDictionary;
        }

        public static void SetDictionary(Dictionary<int, CreatureInfo> dic)
        {
            creatureInfoDictionary = dic;
        }
    }
}
