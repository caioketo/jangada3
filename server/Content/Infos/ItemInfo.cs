﻿using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content.Infos
{
    public class ItemInfo
    {
        public int SpriteId;
        public int Speed;
        public string Description;
        public int DecayTo = 0;
        public long DecayTime = 0;
        public int Volume;
        public Enums.ItemGroup Group;
        public Enums.SlotType slot;

        private static Dictionary<int, ItemInfo> itemInfoDictionary = new Dictionary<int, ItemInfo>();

        public static ItemInfo GetItemInfo(int id)
        {
            return itemInfoDictionary[id];
        }

        public static Dictionary<int, ItemInfo> GetDictionary()
        {
            return itemInfoDictionary;

        }

        public static void SetDictionary(Dictionary<int, ItemInfo> dic)
        {
            itemInfoDictionary = dic;
        }
    }
}
