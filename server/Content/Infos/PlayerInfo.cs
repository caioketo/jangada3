﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content.Infos
{
    public class PlayerInfo
    {
        public int CharacterId;
        public string Name;
        public int AreaId;
        public int Health;
        public int Mana;
        public int Level;
        public Position SavedPosition;
        public int Str;
        public int Cons;
        public int Dex;
        public int Agi;
        public int Int;
        public int Wis;
        public int Experience;
        public int SkillPoints;
        public int ClassId;
        public int RaceId;
        public int StatPoints;
        public Dictionary<int, int> SkillsLearnt;
    }
}
