﻿using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content.Infos
{
    public class ClassInfo
    {
        public ushort Id { get; set; }
        public string Description { get; set; }
        public SkillTree skillTree { get; set; }
        public Dictionary<server.Util.Enums.AttributeType, IAttribute> AttributeDictionary = new Dictionary<server.Util.Enums.AttributeType, IAttribute>();
        private static Dictionary<ushort, ClassInfo> classInfoDictionary = new Dictionary<ushort, ClassInfo>();

        public ClassInfo(ushort id)
        {
            this.Id = id;
            this.skillTree = new SkillTree();
            Dictionary<int, SkillInfo> dic = SkillInfo.GetDictionary();
            List<SkillInfo> list = dic.Values.ToList();
            IEnumerable<SkillInfo> skills = list.Where(
                skill => skill.Class == this.Id);
            var skillsIds = skills.Select(skill => skill.Id);
            skillTree.Skills = skillsIds.ToList();
        }

        public static ClassInfo GetClassInfo(ushort Id)
        {
            return classInfoDictionary[Id];
        }

        public static void SetDictionary(Dictionary<ushort, ClassInfo> dic)
        {
            classInfoDictionary = dic;
        }
    }
}
