﻿using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content.Infos
{
    public class RaceInfo
    {
        public ushort Id { get; set; }
        public string Description { get; set; }
        public Dictionary<Enums.AttributeType, IAttribute> AttributeDictionary = new Dictionary<Enums.AttributeType, IAttribute>();

        private static Dictionary<ushort, RaceInfo> raceInfoDictionary = new Dictionary<ushort, RaceInfo>();

        public RaceInfo(ushort id)
        {
            this.Id = id;
        }

        public static RaceInfo GetRaceInfo(ushort Id)
        {
            return raceInfoDictionary[Id];
        }

        public static void SetDictionary(Dictionary<ushort, RaceInfo> dic)
        {
            raceInfoDictionary = dic;
        }
    }
}
