﻿using server.Content.Infos;
using server.Network;
using server.Network.Packets.Server;
using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content
{
    public class Player : Creature
    {
        public Connection connection { get; set; }


        public int CharacterId;
        public int Level;
        public Position SavedPosition;
        //public Inventory Inventory;
        public int Experience;
        public int SkillPoints;
        public int StatPoints;
        public List<Skill> SkillsLearnt = new List<Skill>();
        public ClassInfo Class;
        public RaceInfo Race;


        public Player(Connection conn) : base()
        {
            connection = conn;
            conn.player = this;
            Pos = new Position(5, 15);
            SkillPoints = 10;
        }

        public void SetInfo(PlayerInfo info)
        {
            this.CharacterId = info.CharacterId;
            this.Name = info.Name;
            this.Level = info.Level;
            this.Pos = info.SavedPosition;
            this.SetStat(Enums.Stat.STR, info.Str);
            this.SetStat(Enums.Stat.CONS, info.Cons);
            this.SetStat(Enums.Stat.AGI, info.Agi);
            this.SetStat(Enums.Stat.DEX, info.Dex);
            this.SetStat(Enums.Stat.INT, info.Int);
            this.SetStat(Enums.Stat.WIS, info.Wis);
            this.Health = this.MaxHealth;
            this.Experience = info.Experience;
            this.SkillPoints = info.SkillPoints;
            this.Class = ClassInfo.GetClassInfo((ushort)info.ClassId);
            this.Race = RaceInfo.GetRaceInfo((ushort)info.RaceId);
            this.StatPoints = info.StatPoints;
            this.Area = Game.Instance.GetAreaById(info.AreaId);
            this.Health = info.Health;
            this.Mana = info.Mana;
            //for (byte i = (byte)SlotType.First; i < (byte)SlotType.Last; i++)
            //{
                //if (info.Inventory.Items.ContainsKey(i))
                //{
                    //this.Inventory.SetItemInSlot(Item.Create(info.Inventory.Items[i]), (SlotType)i);
                //}
            //}

            for (int i = 0; i < info.SkillsLearnt.Count; i++)
            {
                Skill s = Skill.Create((ushort)info.SkillsLearnt.ElementAt(i).Key);
                s.Level = info.SkillsLearnt.ElementAt(i).Value;
                this.AddSkill(s);
            }
        }


        public PlayerInfo GetInfo()
        {
            PlayerInfo info = new PlayerInfo();
            info.CharacterId = this.CharacterId;
            info.Name = this.Name;
            info.Level = this.Level;
            info.SavedPosition = Pos;
            info.Str = GetStat(Enums.Stat.STR);
            info.Dex = GetStat(Enums.Stat.DEX);
            info.Int = GetStat(Enums.Stat.INT);
            info.Cons = GetStat(Enums.Stat.CONS);
            info.Wis = GetStat(Enums.Stat.WIS);
            info.Experience = this.Experience;
            info.SkillPoints = this.SkillPoints;
            info.ClassId = this.Class.Id;
            info.RaceId = this.Race.Id;
            info.StatPoints = this.StatPoints;
            //info.Inventory = new InventoryInfo();
            //foreach (var i in this.Inventory.GetSlotItems())
            //{
                //info.Inventory.Items.Add((byte)i.Key, i.Value.Id);
            //}

            info.SkillsLearnt = new Dictionary<int, int>();

            foreach (Skill s in this.SkillsLearnt)
            {
                info.SkillsLearnt.Add(s.Id, s.Level);
            }

            return info;
        }

        public void AddSkill(Skill skill)
        {
            SkillPoints--;
            SkillsLearnt.Add(skill);
            skill.Info.Script.onInit(skill, this);
            NetworkMessage outMessage = new NetworkMessage();
            NewSkillPacket.Add(outMessage, skill);
            UpdateCreaturePacket.Add(outMessage, GUID, Enums.ThingProp.SkillPoints, SkillPoints);
            connection.Send(outMessage);
        }

        public bool CanLearn(Skill skill)
        {
            if (this.SkillsLearnt.Contains(skill))
            {
                return (SkillPoints > 0);
            }

            var skillsIds = skill.Info.RequiredSkills.Keys.ToArray();
            var skillsLevels = skill.Info.RequiredSkills.Values.ToArray();

            for (int i = 0; i < skillsIds.Count(); i++)
            {
                uint skillId = skillsIds[i];
                Skill s = GetSkill(skillId);
                if (s == null)
                {
                    return false;
                }

                if (s.Level < skillsLevels[i])
                {
                    return false;
                }
            }

            return (SkillPoints > 0);
        }

        public Skill GetSkill(uint id)
        {
            foreach (Skill skill in this.SkillsLearnt)
            {
                if (skill.Id == id)
                {
                    return skill;
                }
            }
            return null;
        }


        public void AddExp(int exp)
        {
            this.Experience += exp;
            uint nextLevelExp = Util.Util.GetNextLevelExp((ushort)this.Level++);

            //WNetworkMessage outMessage = new WNetworkMessage();
            //TextMessagePacket.Add(outMessage, TextMessageType.Default, string.Format("You gained {0} experience!", exp));
            if (nextLevelExp > 0 && this.Experience > nextLevelExp)
            {
                this.Level++;
                //TextMessagePacket.Add(outMessage, TextMessageType.Default, string.Format("You advanced from level {0} to {1}", this.Level - 1, this.Level));
            }
            //PlayerStatusPacket.Add(outMessage, this);
            //ths.connection.Send(outMessage);
        }

        public void AddStatus(int mod, Enums.Stat type, bool forces)
        {
            if ((this.StatPoints >= mod) || forces)
            {
                this.SetStat(type, GetStat(type) + mod);

                if (!forces)
                {
                    this.StatPoints -= (int)mod;
                }

                //WNetworkMessage outMessage = new WNetworkMessage();
                //PlayerStatusPacket.Add(outMessage, this);
                //this.connection.Send(outMessage);
            }
        }

        public void RemoveStatus(int mod, Enums.Stat statType)
        {
            this.SetStat(statType, GetStat(statType) - mod);
            //WNetworkMessage outMessage = new WNetworkMessage();
            //PlayerStatusPacket.Add(outMessage, this);
            //this.connection.Send(outMessage);
        }
    }
}
