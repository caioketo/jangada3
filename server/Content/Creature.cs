﻿using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace server.Content
{
    public delegate uint OnGetAttackSpeed(uint attackSpeed);
    public class Creature : Thing
    {
        public event OnGetAttackSpeed OnGetASHandler;
        public string Name { get; set; }
        public Area Area { get; set; }
        public Config.Direction Direction { get; set; }
        private Dictionary<Enums.Stat, int> Stats = new Dictionary<Enums.Stat, int>();
        public int Health = 1;
        public int Mana = 1;
        private Timer RegenTimer;
        public Creature Target = null;
        public double lastAttkTime = 0;
        public long AttackAnimationTime = 100;

        public int MaxHealth
        {
            get
            {
                return Stats[Enums.Stat.CONS] * 100;
            }
        }

        public int MaxMana
        {
            get
            {
                return Stats[Enums.Stat.WIS] * 100;
            }
        }

        public int AttackSpeed
        {
            get
            {
                return 5000 / Stats[Enums.Stat.AGI];
            }
        }

        public int AttackDamage
        {
            get
            {
                return Stats[Enums.Stat.STR] * 2;
            }
        }


        public int GetStat(Enums.Stat stat)
        {
            return Stats[stat];
        }

        public void SetStat(Enums.Stat stat, int value)
        {
            if (Stats.ContainsKey(stat))
            {
                Stats[stat] = value;
            }
            else
            {
                Stats.Add(stat, value);
            }
        }

        public int Range
        {
            get
            {
                return 1;
            }
        }


        public Creature() : base()
        {
            Name = "Nameless";
            Direction = Config.Direction.E;
            Stats.Add(Enums.Stat.AGI, 1);
            Stats.Add(Enums.Stat.CONS, 1);
            Stats.Add(Enums.Stat.DEX, 1);
            Stats.Add(Enums.Stat.INT, 1);
            Stats.Add(Enums.Stat.STR, 1);
            Stats.Add(Enums.Stat.WIS, 1);
            Health = MaxHealth;

            RegenTimer = new Timer(RegenCallback, this, TimeSpan.FromSeconds(1), TimeSpan.FromSeconds(1));
        }

        public void DoBattle(long gameTick)
        {
            if (Target == null)
            {
                return;
            }   

            //if (lastAttkTime + AttackSpeed > DateTime.Now.TimeOfDay.TotalMilliseconds)
            //{
                //return;
            //}

            if (!Target.Pos.InRange(this.Pos, this.Range))
            {
                return;
            }


            if (this.lastAttkTime + this.GetAttackDuration() < (DateTime.Now.Ticks / 10000))
            {
                this.lastAttkTime = DateTime.Now.Ticks / 10000;
                int damage = AttackDamage;
                Game.Instance.CreatureDamaged(Target, damage, this);
            }
        }

        public int GetAttackDuration()
        {
            // AS goes here
            int baseAS = AttackSpeed;
            if (OnGetASHandler != null)
            {
                foreach (OnGetAttackSpeed onGetAS in OnGetASHandler.GetInvocationList())
                {
                    baseAS += (int)onGetAS.DynamicInvoke(baseAS);
                }
            }

            if (this.IsPlayer)
            {
                Player player = (Player)this;
                //if (player.Class.AttributeDictionary.ContainsKey(AttributeType.AttackSpeed))
                //{
                    //int mod = player.Class.AttributeDictionary[AttributeType.AttackSpeed].GetModifier(baseAS);
                    //mod += (int)baseAS;
                    //baseAS = (uint)mod;
                //}

                //if (player.Race.AttributeDictionary.ContainsKey(AttributeType.AttackSpeed))
                //{
                    //int mod = player.Race.AttributeDictionary[AttributeType.AttackSpeed].GetModifier(baseAS);
                    //mod += (int)baseAS;
                    //baseAS = (uint)mod;
                //}
            }

            return baseAS;
        }

        private static void RegenCallback(object state)
        {
            Creature cThis = (Creature)state;
            int hpReg = cThis.GetStat(Enums.Stat.CONS);
            Game.Instance.CreatureAddHealth(cThis, hpReg);
            int mpReg = cThis.GetStat(Enums.Stat.WIS);
            Game.Instance.CreatureAddMana(cThis, mpReg);
        }



        public override bool Equals(object obj)
        {
            Creature creature = obj as Creature;

            return (creature.GUID.Equals(this.GUID));
        }

        public override int GetHashCode()
        {
            return this.GUID.GetHashCode();
        }

        public void SetTarget(Creature target)
        {
            Target = target;
        }

        public void Walk(Config.Direction direction)
        {
            this.Direction = direction;
            Position delta = Delta(direction, this.Pos.Y % 2 != 0);
            this.Pos.X += delta.X;
            this.Pos.Y += delta.Y;
        }

        public Position Delta(Config.Direction dir, bool oddrow)
        {
            Position delta = new Position(0, 0);
            switch (dir)
            {
                case Config.Direction.NE:
                    if (oddrow)
                    {
                        delta.X = 1;
                    }
                    delta.Y = -1;
                    break;
                case Config.Direction.NW:
                    if (!oddrow)
                    {
                        delta.X = -1;
                    }
                    delta.Y = -1;
                    break;
                case Config.Direction.SW:
                    if (!oddrow)
                    {
                        delta.X = -1;
                    }
                    delta.Y = 1;
                    break;
                case Config.Direction.SE:
                    if (oddrow)
                    {
                        delta.X = 1;
                    }
                    delta.Y = 1;
                    break;
                case Config.Direction.N:
                    delta.Y = -2;
                    break;
                case Config.Direction.S:
                    delta.Y = 2;
                    break;
                case Config.Direction.E:
                    delta.X = 1;
                    break;
                case Config.Direction.W:
                    delta.X = -1;
                    break;
                default:
                    break;
            }
            return delta;
        }

        public bool IsPlayer
        {
            get
            {
                return (this is Player);
            }
        }
    }
}
