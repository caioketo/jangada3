﻿using server.Content.Infos;
using server.Network;
using server.Network.Packets.Server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content
{
    public class Skill
    {
        public int Id;
        public int Level;
        public bool IsActive;
        private long LastUse = 0;

        public SkillInfo Info
        {
            get
            {
                return SkillInfo.GetSkillInfo(Id);
            }
        }

        private Skill(ushort id)
        {
            this.Id = id;
            this.Level = 1;
            this.IsActive = false;
        }

        public static Skill Create(ushort id)
        {
            return new Skill(id);
        }

        public uint GetCooldown()
        {
            return Info.Cooldown;
        }

        public bool OnUse(Player player, Position centerPos)
        {
            if (!this.CanUse()) return false;
            NetworkMessage outMessage = new NetworkMessage();
            this.LastUse = DateTime.Now.Ticks / 10000;

            List<Creature> targets = new List<Creature>();

            if (this.Info.Radius > 0)
            {
                List<Position> area = player.Area.GetTilesInRange(centerPos, this.Info.Radius).ToList();
                foreach (Position pos in area)
                {
                    Creature creature = player.Area.GetCreatureInPos(pos);
                    if (creature != null)
                    {
                        targets.Add(creature);
                    }
                    EffectPacket.Add(outMessage, pos, this.Info.effect);
                }
            }
            else
            {
                targets.Add(player.Target);
            }

            switch (this.Info.Type)
            {
                case server.Util.Enums.SkillType.Activable:
                    if (this.IsActive)
                    {
                        this.Info.Script.onDesactivate();
                    }
                    else
                    {
                        this.Info.Script.onActivate();
                    }
                    return true;
                case server.Util.Enums.SkillType.Damage:
                    int dmg = this.Info.Script.getDamage();
                    foreach (Creature creature in targets)
                    {
                        Game.Instance.CreatureDamaged(creature, dmg, player);
                    }
                    SetSkillCooldownPacket.Add(outMessage, this);
                    player.connection.Send(outMessage);
                    return true;
                case server.Util.Enums.SkillType.Castable:
                    this.Info.Script.onCast();
                    SetSkillCooldownPacket.Add(outMessage, this);
                    player.connection.Send(outMessage);
                    return true;
                case server.Util.Enums.SkillType.Passive:
                    return false;
                default:
                    return false;
            }
        }

        public bool CanUse()
        {
            if (LastUse == 0) return true;

            if (this.LastUse + this.Info.Cooldown < (DateTime.Now.Ticks / 10000))
            {
                return true;
            }

            return false;
        }

        public void AddLevel()
        {
            Level++;
        }
    }
}
