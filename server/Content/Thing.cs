﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Content
{
    public class Thing
    {
        public int SpriteId { get; set; }
        public Guid GUID { get; set; }
        public Position Pos { get; set; }

        public Thing()
        {
            GUID = Guid.NewGuid();
            SpriteId = 0;
            Pos = new Position();
        }

        public override bool Equals(object obj)
        {
            Thing thing = obj as Thing;

            return (thing.GUID.Equals(this.GUID));
        }

        public override int GetHashCode()
        {
            return this.GUID.GetHashCode();
        }
    }
}
