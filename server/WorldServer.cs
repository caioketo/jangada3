﻿using server.Content;
using server.Network;
using server.Network.Packets.Server;
using server.Scripting;
using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server
{
    public class WorldServer
    {
        static void Main(string[] args)
        {
            new WorldServer().Run();
        }

        Server server;

        void Run()
        {
            if (Game.Instance == null)
            {
                new Game(this);
            }
            Loader.LoadAllMaps();
            string scriptErrors = ScriptManager.LoadAllScripts();
            Console.WriteLine(scriptErrors);
            Database.Database.LoadLevel();
            Database.Database.LoadClasses();
            Database.Database.LoadRaces();
            Database.Database.LoadCreatures();
            Database.Database.LoadItens();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(AllHandler);
            server = new Server(ParsePacket);
            server.Run();

            while (true)
            {
                string line = Console.ReadLine();

                if (line == "exit")
                {
                    break;
                }
            }
        }


        public void ParsePacket(byte type, NetworkMessage message, Connection connection)
        {
            NetworkMessage outMessage = new NetworkMessage();
            switch ((MessageType)type)
            {
                case MessageType.LoginPacket:                    
                    Game.Instance.PlayerLogin(connection);
                    break;
                case MessageType.RequestPlayerWalkPacket:
                    Game.Instance.PlayerWalk(connection, (Config.Direction)message.GetByte());
                    break;
                case MessageType.ChangeDirPacket:
                    Game.Instance.ChangeDir(connection, (Config.Direction)message.GetByte());
                    break;
                case MessageType.RequestSetTargetPacket:
                    Game.Instance.SetTarget(connection, message.GetGuid());
                    break;
                case MessageType.LearnSkill:
                    Game.Instance.PlayerLearnSkill(connection, message.GetUInt16());
                    break;
                case MessageType.UseSkill:
                    ParsePlayerUseSkill(connection, message);
                    break;
            }
        }

        private void ParsePlayerUseSkill(Connection connection, NetworkMessage message)
        {
            uint skillId = message.GetUInt32();
            Position centerPos = null;
            if (message.GetByte() == 0x01)
            {
                centerPos = message.GetPosition();
            }


            Game.Instance.PlayerUseSkill(connection, skillId, centerPos);
        }

        public void SendInitialPacket(Player player)
        {
            NetworkMessage outMessage = new NetworkMessage();
            PlayerLoginPacket.Add(outMessage, player);
            AreaDescriptionPacket.Add(outMessage, player);
            SkillTreePacket.Add(outMessage, player);
            player.connection.Send(outMessage, true);
            player.connection.InitialPacketSent = true;
        }

        static void AllHandler(object sender, UnhandledExceptionEventArgs args)
        {
            Exception e = (Exception)args.ExceptionObject;
            Console.WriteLine("AllHandler caught : " + e.Message);
        }
    }
}
