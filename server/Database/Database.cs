﻿using MySql.Data.MySqlClient;
using server.Content.Infos;
using server.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace server.Database
{
    public class Database
    {
        #region LEVELS
        static string SELECT_LEVEL = "SELECT level.level, level.experience FROM level ORDER BY level.level;";
        static int LEVEL_LEVEL = 0;
        static int LEVEL_EXPERIENCE = 1;
        #endregion

        #region CLASSES
        static string SELECT_CLASSES = "SELECT classes.id, classes.description FROM classes;";
        static int CLASSES_ID = 0;
        static int CLASSES_DESCRIPTION = 1;
        static string SELECT_MODIFIERS_CLASSES = "SELECT modifiers.type, modifiers.percent FROM modifiers WHERE modifiers.id in (SELECT classes_modifiers.id_modifier FROM classes_modifiers WHERE classes_modifiers.id_class = ?);";
        static int MODIFIERS_TYPE = 0;
        static int MODIFIERS_PERCENT = 1;
        #endregion

        #region RACES
        static string SELECT_RACES = "SELECT races.id, races.description FROM races;";
        static int RACES_ID = 0;
        static int RACES_DESCRIPTION = 1;
        static string SELECT_MODIFIERS_RACES = "SELECT modifiers.type, modifiers.percent FROM modifiers WHERE modifiers.id in (SELECT races_modifiers.id_modifier FROM races_modifiers WHERE races_modifiers.id_race = ?);";
        #endregion

        #region CREATURES
        static string SELECT_CREATURES = "SELECT id, name, experience, bodyId, str, cons, agi, dex, inteligence, wis FROM creatures ";
        static int CREATURE_ID = 0;
        static int CREATURE_NAME = 1;
        static int CREATURE_EXPERIENCE = 2;
        static int CREATURE_BODYID = 3;
        static int CREATURE_STR = 4;
        static int CREATURE_CONS = 5;
        static int CREATURE_AGI = 6;
        static int CREATURE_DEX = 7;
        static int CREATURE_INT = 8;
        static int CREATURE_WIS = 9;
        static string SELECT_LOOTS_CREATURE = "SELECT loot.itemId, loot.chance FROM loot WHERE loot.id in (SELECT crature_loot.loot_id FROM creature_loot WHERE creature_loot.creature_id = ?);";
        static int LOOT_ITEM_ID = 0;
        static int LOOT_CHANCE = 1;
        #endregion

        #region ITENS
        static string SELECT_ITENS = "SELECT id, sprite_id, speed, description, decayTo, decayTime, volume, item_group, slot FROM itens ";
        static int ITEM_ID = 0;
        static int ITEM_SPRITE_ID = 1;
        static int ITEM_SPEED = 2;
        static int ITEM_DESCRIPTION = 3;
        static int ITEM_DECAYTO = 4;
        static int ITEM_DECAYTIME = 5;
        static int ITEM_VOLUME = 6;
        static int ITEM_GROUP = 7;
        static int ITEM_SLOT = 8;
        #endregion

        #region PLAYER
        static string SELECT_PLAYER = "select name, level, posX, posY, areaId, str, cons, agi, dex, inteligence, wis, health, mana, experience, skillPoints, statsPoints, race, class from players where id = ?";
        static int PLAYER_NAME = 0;
        static int PLAYER_LEVEL = 1;
        static int PLAYER_POS_X = 2;
        static int PLAYER_POS_Y = 3;
        static int PLAYER_AREA_ID = 4;
        static int PLAYER_STR = 5;
        static int PLAYER_CONS = 6;
        static int PLAYER_AGI = 7;
        static int PLAYER_DEX = 8;
        static int PLAYER_INT = 9;
        static int PLAYER_WIS = 10;
        static int PLAYER_HEALTH = 11;
        static int PLAYER_MANA = 12;
        static int PLAYER_EXPERIENCE = 13;
        static int PLAYER_SKILL_POINTS = 14;
        static int PLAYER_STATS_POINTS = 15;
        static int PLAYER_RACE = 16;
        static int PLAYER_CLASS = 17;

        static string SELECT_SKILLS = "select skill_id, skill_level from skills_learnt where player_id = ?";
        static int SKILL_ID = 0;
        static int SKILL_LEVEL = 1;
        #endregion



        static string CONNECTION = "Server=localhost; database=jangada; UID=root; password=vd001989";


        public static PlayerInfo LoadPlayer(int id)
        {
            PlayerInfo info = new PlayerInfo();
            using (MySqlConnection conn = new MySqlConnection(CONNECTION))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SELECT_PLAYER, conn))
                {
                    MySqlParameter param = new MySqlParameter();
                    param.Value = id;
                    cmd.Parameters.Add(param);
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            info.Name = reader.GetString(PLAYER_NAME);
                            info.Level = reader.GetInt32(PLAYER_LEVEL);
                            info.SavedPosition = new Content.Position(reader.GetInt32(PLAYER_POS_X), reader.GetInt32(PLAYER_POS_Y));
                            info.AreaId = reader.GetInt32(PLAYER_AREA_ID);
                            info.Str = reader.GetInt32(PLAYER_STR);
                            info.Cons = reader.GetInt32(PLAYER_CONS);
                            info.Agi = reader.GetInt32(PLAYER_AGI);
                            info.Dex = reader.GetInt32(PLAYER_DEX);
                            info.Int = reader.GetInt32(PLAYER_INT);
                            info.Wis = reader.GetInt32(PLAYER_WIS);
                            info.Health = reader.GetInt32(PLAYER_HEALTH);
                            info.Mana = reader.GetInt32(PLAYER_MANA);
                            info.Experience = reader.GetInt32(PLAYER_EXPERIENCE);
                            info.SkillPoints = reader.GetInt32(PLAYER_SKILL_POINTS);
                            info.StatPoints = reader.GetInt32(PLAYER_STATS_POINTS);
                            info.RaceId = reader.GetInt32(PLAYER_RACE);
                            info.ClassId = reader.GetInt32(PLAYER_CLASS);
                        }
                    }
                    info.SkillsLearnt = new Dictionary<int, int>();
                    cmd.CommandText = SELECT_SKILLS;
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            info.SkillsLearnt.Add(reader.GetInt32(SKILL_ID), reader.GetInt32(SKILL_LEVEL));
                        }
                    }
                }
            }
            return info;
        }

        public static void LoadRaces()
        {
            Dictionary<ushort, RaceInfo> dictionary = new Dictionary<ushort, RaceInfo>();
            using (MySqlConnection conn = new MySqlConnection(CONNECTION))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SELECT_RACES, conn))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            RaceInfo info = new RaceInfo((ushort)reader.GetInt32(RACES_ID));
                            info.Description = reader.GetString(RACES_DESCRIPTION);
                            using (MySqlConnection conn2 = new MySqlConnection(CONNECTION))
                            {
                                conn2.Open();
                                using (MySqlCommand cmd2 = new MySqlCommand(SELECT_MODIFIERS_RACES, conn2))
                                {
                                    MySqlParameter param = new MySqlParameter();
                                    param.Value = reader.GetInt32(RACES_ID);
                                    cmd2.Parameters.Add(param);
                                    using (MySqlDataReader reader_modifiers = cmd2.ExecuteReader())
                                    {
                                        while (reader_modifiers.Read())
                                        {
                                            server.Util.Enums.AttributeType type = Util.Util.ParseAttributeType(reader_modifiers.GetString(MODIFIERS_TYPE));
                                            info.AttributeDictionary.Add(type, new ModifyAttribute(reader_modifiers.GetInt32(MODIFIERS_PERCENT)));
                                        }
                                    }
                                }
                            }

                            dictionary.Add(info.Id, info);
                        }
                    }
                }
            }

            RaceInfo.SetDictionary(dictionary);
        }

        public static void LoadClasses()
        {
            Dictionary<ushort, ClassInfo> dictionary = new Dictionary<ushort, ClassInfo>();
            using (MySqlConnection conn = new MySqlConnection(CONNECTION))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SELECT_CLASSES, conn))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ClassInfo info = new ClassInfo((ushort)reader.GetInt32(CLASSES_ID));
                            info.Description = reader.GetString(CLASSES_DESCRIPTION);
                            using (MySqlConnection conn2 = new MySqlConnection(CONNECTION))
                            {
                                conn2.Open();
                                using (MySqlCommand cmd2 = new MySqlCommand(SELECT_MODIFIERS_CLASSES, conn2))
                                {
                                    MySqlParameter param = new MySqlParameter();
                                    param.Value = reader.GetInt32(CLASSES_ID);
                                    cmd2.Parameters.Add(param);
                                    using (MySqlDataReader reader_modifiers = cmd2.ExecuteReader())
                                    {
                                        while (reader_modifiers.Read())
                                        {
                                            server.Util.Enums.AttributeType type = Util.Util.ParseAttributeType(reader_modifiers.GetString(MODIFIERS_TYPE));
                                            info.AttributeDictionary.Add(type, new ModifyAttribute(reader_modifiers.GetInt32(MODIFIERS_PERCENT)));
                                        }
                                    }
                                }
                            }

                            dictionary.Add(info.Id, info);
                        }
                    }
                }
            }

            ClassInfo.SetDictionary(dictionary);
        }

        public static void LoadLevel()
        {
            Dictionary<ushort, uint> dic = new Dictionary<ushort, uint>();
            using (MySqlConnection conn = new MySqlConnection(CONNECTION))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SELECT_LEVEL, conn))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            dic.Add((ushort)reader.GetInt32(LEVEL_LEVEL), (uint)reader.GetInt32(LEVEL_EXPERIENCE));
                        }
                    }
                }
            }

            Util.Util.ExpByLevel = dic;
        }

        public static void LoadItens()
        {
            Dictionary<int, ItemInfo> dic = new Dictionary<int, ItemInfo>();
            using (MySqlConnection conn = new MySqlConnection(CONNECTION))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SELECT_ITENS, conn))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            ItemInfo info = new ItemInfo();
                            info.SpriteId = reader.GetInt32(ITEM_SPRITE_ID);
                            info.Speed = reader.GetInt32(ITEM_SPEED);
                            info.Description = reader.GetString(ITEM_DESCRIPTION);
                            if (!reader.IsDBNull(ITEM_DECAYTO))
                            {
                                info.DecayTo = reader.GetInt32(ITEM_DECAYTO);
                            }

                            if (!reader.IsDBNull(ITEM_DECAYTIME))
                            {
                                info.DecayTime = reader.GetInt32(ITEM_DECAYTIME);
                            }

                            if (!reader.IsDBNull(ITEM_VOLUME))
                            {
                                info.Volume = reader.GetInt32(ITEM_VOLUME);
                            }
                            info.Group = (Enums.ItemGroup)reader.GetInt32(ITEM_GROUP);
                            if (!reader.IsDBNull(ITEM_SLOT))
                            {
                                info.slot = (Enums.SlotType)reader.GetInt32(ITEM_SLOT);
                            }

                            dic.Add(reader.GetInt32(ITEM_ID), info);
                        }
                    }
                }
            }

            ItemInfo.SetDictionary(dic);
        }


        public static void LoadCreatures()
        {
            Dictionary<int, CreatureInfo> dictionary = new Dictionary<int, CreatureInfo>();

            using (MySqlConnection conn = new MySqlConnection(CONNECTION))
            {
                conn.Open();
                using (MySqlCommand cmd = new MySqlCommand(SELECT_CREATURES, conn))
                {
                    using (MySqlDataReader reader = cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CreatureInfo info = new CreatureInfo();
                            info.BodyId = reader.GetInt32(CREATURE_BODYID);
                            info.Experience = reader.GetInt32(CREATURE_EXPERIENCE);
                            info.Name = reader.GetString(CREATURE_NAME);
                            info.Stats.Add(Enums.Stat.STR, reader.GetInt32(CREATURE_STR));
                            info.Stats.Add(Enums.Stat.CONS, reader.GetInt32(CREATURE_CONS));
                            info.Stats.Add(Enums.Stat.AGI, reader.GetInt32(CREATURE_AGI));
                            info.Stats.Add(Enums.Stat.DEX, reader.GetInt32(CREATURE_DEX));
                            info.Stats.Add(Enums.Stat.INT, reader.GetInt32(CREATURE_INT));
                            info.Stats.Add(Enums.Stat.WIS, reader.GetInt32(CREATURE_WIS));

                            using (MySqlConnection conn2 = new MySqlConnection(CONNECTION))
                            {
                                conn2.Open();
                                using (MySqlCommand cmd2 = new MySqlCommand(SELECT_LOOTS_CREATURE, conn2))
                                {
                                    cmd2.Parameters.Add(reader.GetInt32(CREATURE_ID));
                                    using (MySqlDataReader reader_loots = cmd2.ExecuteReader())
                                    {
                                        while (reader_loots.Read())
                                        {
                                            info.loot.Ids.Add(reader_loots.GetInt32(LOOT_ITEM_ID));
                                            info.loot.Chances.Add(reader_loots.GetInt32(LOOT_CHANCE));
                                        }
                                    }
                                }
                            }


                            dictionary.Add(reader.GetInt32(CREATURE_ID), info);
                        }
                    }
                }
            }


            CreatureInfo.SetDictionary(dictionary);
        }
    }
}
