﻿using server.Content;
using server.Content.Infos;
using server.Network;
using server.Network.Packets.Server;
using server.Util;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;

namespace server
{
    public class Game
    {
        private static Game instance;
        public static Game Instance
        {
            get
            {
                return instance;
            }
        }

        public WorldServer worldServer;
        public List<Player> Players = new List<Player>();
        public List<Area> Areas = new List<Area>();
        private System.Timers.Timer GameTickTimer;
        public Stopwatch stopWatch = new Stopwatch();
        public long GameTick = 0;

        public Game(WorldServer server)
        {
            instance = this;
            worldServer = server;

            GameTickTimer = new System.Timers.Timer();
            GameTickTimer.Interval = 1;
            GameTickTimer.Elapsed += GameTickCallback;
            GameTickTimer.AutoReset = false;
            GameTickTimer.Start();     
        }

        private static void GameTickCallback(object sender, System.Timers.ElapsedEventArgs e)
        {
            Game game = Game.instance;
            if (game.stopWatch.IsRunning)
            {
                game.stopWatch.Stop();
                game.GameTick += game.stopWatch.ElapsedMilliseconds;
                game.stopWatch.Start();
            }

            foreach (Area area in game.Areas)
            {
                area.Tick(game.GameTick);
                game.stopWatch.Stop();
                game.GameTick += game.stopWatch.ElapsedMilliseconds;
                game.stopWatch.Start();
            }
            game.GameTickTimer.Start();//restart the timer
        }

        public Area GetAreaById(int id)
        {
            return Areas.Where(a => a.Id == id).FirstOrDefault();
        }

        public void PlayerLogin(Connection connection)
        {
            Player player = new Player(connection);
            player.SetInfo(Database.Database.LoadPlayer(1));
            player.Area = GetAreaById(1);
            player.Area.AddPlayer(player);
            worldServer.SendInitialPacket(player);
            NetworkMessage repMessage = new NetworkMessage();
            PlayerLoginPacket.Add(repMessage, player);
            foreach (Player player2 in connection.player.Area.GetPlayerExcept(player))
            {
                player2.connection.Send(repMessage);
            }
        }

        internal void PlayerWalk(Connection connection, Config.Direction p)
        {
            connection.player.Walk(p);
            NetworkMessage outMessage = new NetworkMessage();
            PlayerWalkPacket.Add(outMessage, connection.player);
            connection.Send(outMessage);

            NetworkMessage repMessage = new NetworkMessage();
            PlayerMovePacket.Add(repMessage, connection.player);
            UpdateCreaturePacket.Add(repMessage, connection.player.GUID, Util.Enums.ThingProp.Direction, p);
            foreach (Player player in connection.player.Area.GetPlayerExcept(connection.player))
            {
                player.connection.Send(repMessage);
            }
        }

        internal void ChangeDir(Connection connection, Config.Direction p)
        {
            connection.player.Direction = p;
            NetworkMessage repMessage = new NetworkMessage();
            UpdateCreaturePacket.Add(repMessage, connection.player.GUID, Util.Enums.ThingProp.Direction, p);
            foreach (Player player in connection.player.Area.GetPlayerExcept(connection.player))
            {
                player.connection.Send(repMessage);
            }
        }

        internal void CreatureDamaged(Creature creature, int value, Creature attacker)
        {
            if (creature.Health > value)
            {
                creature.Health -= value;
                SendUpdatePacket(creature, Enums.ThingProp.Health, creature.Health);
            }
            else
            {
                //DEAD
            }
        }


        internal void SendUpdatePacket(Creature creature, Enums.ThingProp prop, object value)
        {
            Network.NetworkMessage outMessage = new Network.NetworkMessage();
            Network.Packets.Server.UpdateCreaturePacket.Add(outMessage, creature.GUID, prop, value);
            if (creature is Player)
            {
                ((Player)creature).connection.Send(outMessage);
            }
            foreach (Player player in creature.Area.GetPlayers())
            {
                player.connection.Send(outMessage);
            }
        }


        internal void CreatureAddHealth(Creature creature, int value)
        {
            if (creature.Health == creature.MaxHealth || creature.Health + value >= creature.MaxHealth)
            {
                return;
            }
            creature.Health += value;
            SendUpdatePacket(creature, Enums.ThingProp.Health, creature.Health);
        }

        internal void CreatureAddMana(Creature creature, int value)
        {
            if (creature.Mana == creature.MaxMana || creature.Mana + value >= creature.MaxMana)
            {
                return;
            }
            creature.Mana += value;
            SendUpdatePacket(creature, Enums.ThingProp.Mana, creature.Mana);
        }

        internal void SetTarget(Connection connection, Guid guid)
        {
            connection.player.SetTarget(connection.player.Area.GetCreatureByGUID(guid));
        }

        internal void PlayerLearnSkill(Connection connection, ushort skillId)
        {
            Skill skill = Skill.Create(skillId);
            if (connection.player.CanLearn(skill))
            {
                connection.player.AddSkill(skill);
            }
        }

        internal void PlayerUseSkill(Connection connection, uint skillId, Position centerPos)
        {
            Skill skill = connection.player.SkillsLearnt.Where(s => s.Id == skillId).FirstOrDefault();
            if (skill != null)
            {
                skill.OnUse(connection.player, centerPos);
            }
        }
    }
}
