﻿using cliente.Engine;
using cliente.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Content
{
    public class Effect
    {
        public Point Position;
        public Enums.EffectType Type;
        public AnimateSprite sprite;
        public bool Enabled = true;

        public Effect(Point pos, Enums.EffectType type)
        {
            Position = pos;
            Type = type;
            sprite = Config.CreateEffectSprite(type);
            sprite.onEnd += sprite_onEnd;
            Enabled = true;
        }

        void sprite_onEnd()
        {
            this.Enabled = false;
        }

        internal void Draw(SpriteBatch spriteBatch, Point pos)
        {
            sprite.Draw(spriteBatch, pos);
        }
    }
}
