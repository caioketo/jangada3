﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Content
{
    public class AoE
    {
        public Point Center { get; set; }
        public double Radius { get; set; }
        public List<Point> Area { get; set; }

        public AoE(double radius)
        {
            Radius = radius;
        }

        public void Calculate()
        {
            if (Radius == 0)
            {
                Area = new List<Point>();
                Area.Add(Center);
            }
            else
            {
                Area = Game1.GetMap().GetTilesInRange(Center, Radius).ToList();
            }
        }
    }
}
