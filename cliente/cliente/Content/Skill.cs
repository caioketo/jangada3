﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace cliente.Content
{
    public class Skill
    {
        public int Id;
        public int Level;
        public bool Targetable;
        public int Cooldown;
        public int InCooldown;
        public AoE AoE;
        public Dictionary<Point, ushort> AOE = new Dictionary<Point, ushort>();
        private Timer cdTimer = new Timer();

        public Skill()
        {
            cdTimer.Elapsed += (sender, e) => OnTimedEvent(sender, e, this);
            cdTimer.Enabled = false;
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e, Skill thisS)
        {
            thisS.InCooldown--;
            if (thisS.InCooldown <= 1)
            {
                thisS.InCooldown = 0;
                thisS.cdTimer.Enabled = false;
            }
        }

        public void StartCooldown()
        {
            InCooldown = 19;
            double t = (double)Cooldown / 19;
            cdTimer.Interval = t * 1000;
            cdTimer.Start();
        }

        public void Draw(SpriteBatch spriteBatch, Point position)
        {
            spriteBatch.Draw(Game1.GetInstance().skillsTileset.GetTexture(), new Rectangle(position.X, position.Y, 50, 50),
                Game1.GetInstance().skillsTileset.TileByIndex(Id), Color.White);
            if (InCooldown > 0)
            {
                int cdPos = 20 - InCooldown;
                spriteBatch.Draw(Game1.GetInstance().cooldownTileset.GetTexture(), new Rectangle(position.X, position.Y, 50, 50),
                    Game1.GetInstance().cooldownTileset.TileByIndex(cdPos), Color.White);
            }
        }
    }
}
