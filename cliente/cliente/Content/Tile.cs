﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente
{
    public class Tile
    {
        public int Ground { get; set; }

        public Rectangle Rect { get; set; }
    }
}
