﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente
{
    public class Thing
    {
        public TileSet tileset;
        public Guid GUID;
        public Point position;
        public int spriteId = 0;
        public Rectangle Rect;

        public Thing()
        {
            position = new Point(5, 10);
            tileset = Game1.GetInstance().thingsTileset;
        }

        
        public virtual void Draw(SpriteBatch spriteBatch, Point screenPos)
        {
            DrawToScreen(spriteBatch, screenPos);
        }


        public void DrawToScreen(SpriteBatch spriteBatch, Point screenPos)
        {
            Rect = new Rectangle((screenPos.X - (tileset.GetWidth() - (Config.TILE_SIZE / 2))),
                (screenPos.Y - (tileset.GetHeight() - (Config.TILE_SIZE / 2))),
                tileset.GetWidth(), tileset.GetHeight());
            spriteBatch.Draw(tileset.GetTexture(), Rect, tileset.TileByIndex(spriteId),
                            Color.White);
        }
        public override bool Equals(object obj)
        {
            Thing thing = obj as Thing;

            return (thing.GUID.Equals(this.GUID));
        }

        public override int GetHashCode()
        {
            return this.GUID.GetHashCode();
        }
    }
}
