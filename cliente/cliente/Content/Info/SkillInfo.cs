﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Content.Info
{
    public class SkillInfo
    {
        public int Id;
        public int LevelRequired;
        public int PositionInTree;
        public Dictionary<ushort, uint> RequiredSkills = new Dictionary<ushort, uint>();

        public List<SkillInfo> Childs
        {
            get
            {
                return Game1.player.SkillTree.Where(s => s.RequiredSkills.ContainsKey((ushort)this.Id)).ToList();
            }
        }

        public bool IsLearnt
        {
            get
            {
                return Game1.player.Skills.Where(s => s.Id == this.Id).ToList().Count > 0;
            }
        }

        public bool CanLearn
        {
            get
            {
                if (this.LevelRequired > Game1.player.Level)
                {
                    return false;
                }
                foreach (ushort skillId in this.RequiredSkills.Keys)
                {
                    uint reqLvl = RequiredSkills[skillId];
                    if (!Game1.player.IsLearnt(skillId, reqLvl))
                    {
                        return false;
                    }
                }
                return (Game1.player.SkillPoints > 0);
            }
        }

        public override bool Equals(object obj)
        {
            SkillInfo skill = obj as SkillInfo;

            return (skill.Id.Equals(this.Id));
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }
    }
}
