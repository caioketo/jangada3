﻿using cliente.Content;
using cliente.Content.Info;
using cliente.Network.Packets.Client;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente
{
    public class Player : Creature
    {
        public uint Level { get; set; }
        public uint Experience { get; set; }
        public uint SkillPoints{ get; set; }
        public uint StatsPoints { get; set; }
        public uint ClassId { get; set; }
        public uint RaceId { get; set; }
        public List<SkillInfo> SkillTree = new List<SkillInfo>();
        public List<Skill> Skills = new List<Skill>();


        public Player() : base()
        {
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Point screenPos = ConvertCoord(5, 19);
            DrawToScreen(spriteBatch, screenPos);
            Game1.GetInstance().playerHUD.Draw(spriteBatch);
            if (Target != null)
            {
                Game1.GetInstance().targetHUD.Draw(spriteBatch);
            }
        }

        public void SetTarget(Creature _target)
        {
            Target = _target;
            Network.NetworkMessage message = new Network.NetworkMessage();
            RequestSetTargetPacket.Add(message, Target);
            Game1.GetConnection().Send(message);
        }

        internal bool IsLearnt(ushort skillId, uint reqLvl)
        {
            foreach (Skill skill in Skills)
            {
                if (skill.Id == skillId)
                {
                    return skill.Level >= reqLvl;
                }
            }
            return false;
        }
    }
}
