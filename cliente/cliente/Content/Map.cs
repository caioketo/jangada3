﻿using cliente.Content;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente
{
    public class Map
    {
        public int Width = 0;
        public int Height = 0;
        private Dictionary<Point, Tile> tiles;
        public Dictionary<Point, Thing> Things = new Dictionary<Point, Thing>();
        public List<Creature> Creatures = new List<Creature>();
        public List<Effect> Effects = new List<Effect>();

        public Map()
        {
            tiles = new Dictionary<Point, Tile>();
        }

        public Tile GetTile(Point pos)
        {
            Tile t;
            tiles.TryGetValue(pos, out t);
            return t;
        }


        internal void SetTile(Tile tile, int x, int y)
        {
            tiles.Add(new Point(x, y), tile);
        }

        internal Thing GetThing(Point point)
        {
            Thing t;
            Things.TryGetValue(point, out t);
            return t;
        }

        internal Creature GetCreature(Point point)
        {
            return Creatures.Where(c => c.position.Equals(point)).FirstOrDefault();
        }

        internal Creature GetCreatureByGUID(Guid guid)
        {
            return Creatures.Where(c => c.GUID.Equals(guid)).FirstOrDefault();
        }


        internal IEnumerable<Point> GetTilesInRange(Point p1, double radius)
        {
            return tiles.Keys.Where(p2 => Distance(p1, p2) <= radius);
        }


        internal int Distance(Point p1, Point p2)
        {
            bool NSW = ((p2.X < p1.X) || (p2.X == p1.X && p2.Y % 2 == 0 && p1.Y % 2 != 0));
            double difY = Math.Abs(p1.Y - p2.Y);
            double difX = Math.Abs(p1.X - p2.X);
            double dist = (difY / 2) + difX;
            if (NSW)
            {
                if (p1.Y % 2 == 0)
                {
                    return (int)Math.Floor(dist);
                }
                else
                {
                    
                    return (int)Math.Ceiling(dist);
                }
            }
            else
            {
                if (p1.Y % 2 == 0)
                {
                    return (int)Math.Ceiling(dist);
                }
                else
                {
                    return (int)Math.Floor(dist);
                }
            }
        }


        internal void CalculateDistToPlayer()
        {
            foreach (Creature creature in Creatures)
            {
                creature.DistToPlayer = Convert.ToInt32(Math.Floor((double)Math.Abs(Game1.player.position.Y - creature.position.Y) / 2 + Math.Abs(Game1.player.position.X - creature.position.X)));
            }
        }

        internal Creature GetClosestTarget()
        {
            CalculateDistToPlayer();
            List<Creature> creaturesClose = Creatures.OrderBy(c => c.DistToPlayer).ToList();
            if (creaturesClose.Count == 0)
            {
                return null;
            }
            return creaturesClose[0];
        }

        internal IEnumerable<Effect> GetEffects()
        {
            return Effects.Where(e => e.Enabled).ToList();
        }
    }
}
