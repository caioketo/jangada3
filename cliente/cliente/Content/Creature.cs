﻿using cliente.Engine;
using cliente.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace cliente
{
    public class Creature : Thing
    {
        protected Config.Direction direction;
        public string Name;
        public int offsetX = 0;
        public int offsetY = 0;
        public bool incX = true;
        public bool diagonal = false;
        public bool IsWalking = false;
        private Timer walkingTimer = new Timer();
        private Dictionary<Enums.Stat, int> Stats = new Dictionary<Enums.Stat, int>();
        public int Health = 50;
        public int Mana = 1;
        public Creature Target = null;
        public double DistToPlayer = -1;

        public int MaxHealth
        {
            get
            {
                return Stats[Enums.Stat.CONS] * 100;
            }
        }

        public int MaxMana
        {
            get
            {
                return Stats[Enums.Stat.WIS] * 100;
            }
        }
        public void SetStat(Enums.Stat stat, int value)
        {
            if (Stats.ContainsKey(stat))
            {
                Stats[stat] = value;
            }
            else
            {
                Stats.Add(stat, value);
            }
        }

        public bool IsTarget()
        {
            if (Game1.player.Target == null)
            {
                return false;
            }
            return (Game1.player.Target.Equals(this));
        }


        public int GetStat(Enums.Stat stat)
        {
            return Stats[stat];
        }

        public Creature()
        {
            Name = "";
            tileset = Game1.GetInstance().creaturesTileSet;
            direction = Config.Direction.SE;
            position = new Point(5, 15);
            walkingTimer.Elapsed += (sender, e) => OnTimedEvent(sender, e, this);
            walkingTimer.Interval = 10;
            walkingTimer.Enabled = false;
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e, Creature thisC)
        {
            if (thisC.offsetX != 0)
            {
                if (thisC.offsetX > 0)
                {
                    thisC.offsetX--;
                }
                else
                {
                    thisC.offsetX++;
                }
            }
            if (thisC.offsetY != 0 && thisC.incX)
            {
                if (thisC.offsetY > 0)
                {
                    thisC.offsetY--;
                }
                else
                {
                    thisC.offsetY++;
                }
            }
            if (thisC.diagonal)
            {
                thisC.incX = !thisC.incX;
            }
            if (thisC.offsetY == 0 && thisC.offsetX == 0)
            {
                thisC.walkingTimer.Enabled = false;
                thisC.incX = true;
                thisC.IsWalking = false;
            }
        }

        public void LoadTileset()
        {
            tileset = Game1.GetInstance().GetTileSetCreature(spriteId);
        }

        protected Point ConvertCoord(int mapX, int mapY)
        {
            int screenX = mapX * (Config.TILE_SIZE / 2) + ((mapY % 2 == 0 ? 0 : 1) * (Config.TILE_SIZE / 4));
            int screenY = mapY * (Config.TILE_SIZE / 8);
            return new Point(screenX, screenY);
        }

        public void SetDir(Config.Direction dir)
        {
            this.direction = dir;
            this.spriteId = (int)dir + 1;
        }

        public Config.Direction GetDir()
        {
            return this.direction;
        }

        public void Move(Point pos)
        {
            int ofX = 0;
            int ofY = 0;
            Point delta = new Point(pos.X - this.position.X, pos.Y - this.position.Y);
            if (delta.X == 1 && delta.Y == 0)
            {
                ofX = -64;
            }
            else if (delta.X == -1 && delta.Y == 0)
            {
                ofX = 64;
            }
            else if (delta.Y == 2 && delta.X == 0)
            {
                ofY = -32;
            }
            else if (delta.Y == -2 && delta.X == 0)
            {
                ofY = 32;
            }
            else if (delta.Y == 1 && delta.X == 0 && pos.Y % 2 != 0)
            {
                ofX = -32;
                ofY = -16;
                incX = false;
                diagonal = true;
            }
            else if (delta.Y == 1 && delta.X == 0 && pos.Y % 2 == 0)
            {
                ofX = 32;
                ofY = -16;
                incX = false;
                diagonal = true;
            }
            else if (delta.Y == -1 && delta.X == 0 && pos.Y % 2 == 0)
            {
                ofX = 32;
                ofY = 16;
                incX = false;
                diagonal = true;
            }
            else if (delta.Y == -1 && delta.X == 0 && pos.Y % 2 != 0)
            {
                ofX = -32;
                ofY = 16;
                incX = false;
                diagonal = true;
            }
            else if (delta.Y == -1 && delta.X == 1 && pos.Y % 2 == 0)
            {
                ofX = -32;
                ofY = 16;
                incX = false;
                diagonal = true;
            }
            else if (delta.Y == 1 && delta.X == -1 && pos.Y % 2 != 0)
            {
                ofX = 32;
                ofY = -16;
                incX = false;
                diagonal = true;
            }
            else if (delta.Y == 1 && delta.X == 1 && pos.Y % 2 == 0)
            {
                ofX = -32;
                ofY = -16;
                incX = false;
                diagonal = true;
            }
            else if (delta.Y == -1 && delta.X == -1 && pos.Y % 2 != 0)
            {
                ofX = 32;
                ofY = 16;
                incX = false;
                diagonal = true;
            }
            if (this is Player)
            {
                ofX *= -1;
                ofY *= -1;
            }
            this.offsetX = ofX;
            this.offsetY = ofY;
            this.position = pos;
            this.IsWalking = true;
            walkingTimer.Enabled = true;
        }

        override
        public void Draw(SpriteBatch spriteBatch, Point screenPos)
        {
            screenPos.X += offsetX;
            screenPos.Y += offsetY;
            base.Draw(spriteBatch, screenPos);
            if (IsTarget())
            {
                //Draw target glow
            }
        }


        public void Move()
        {
            Point delta = Delta(direction, position.Y % 2 != 0);
            position.X += delta.X;
            position.Y += delta.Y;
        }

        public Point Delta(Config.Direction dir, bool oddrow)
        {
            Point delta = new Point(0, 0);
            switch (dir)
            {
                case Config.Direction.NE:
                    if (oddrow)
                    {
                        delta.X = 1;
                    }
                    delta.Y = -1;
                    break;
                case Config.Direction.NW:
                    if (!oddrow)
                    {
                        delta.X = -1;
                    }
                    delta.Y = -1;
                    break;
                case Config.Direction.SW:
                    if (!oddrow)
                    {
                        delta.X = -1;
                    }
                    delta.Y = 1;
                    break;
                case Config.Direction.SE:
                    if (oddrow)
                    {
                        delta.X = 1;
                    }
                    delta.Y = 1;
                    break;
                case Config.Direction.N:
                    delta.Y = -2;
                    break;
                case Config.Direction.S:
                    delta.Y = 2;
                    break;
                case Config.Direction.E:
                    delta.X = 1;
                    break;
                case Config.Direction.W:
                    delta.X = -1;
                    break;
                default:
                    break;
            }
            return delta;
        }
    }
}
