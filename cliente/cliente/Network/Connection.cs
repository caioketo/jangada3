﻿using cliente.Content;
using cliente.Content.Info;
using cliente.Engine;
using cliente.Network.Packets.Server;
using cliente.Util;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace cliente.Network
{
    public class Connection
    {
        private static Connection instance;
        public static Connection getInstance()
        {
            return instance;
        }

        private const int SleepTimeout = 50;// miliseconds

        Socket socket;
        NetworkMessage inMessage = new NetworkMessage();
        protected NetworkStream stream;
        public Connection()
        {
            instance = this;
        }

        public void StartConnect()
        {
            try
            {
                //We are using TCP sockets
                socket = new Socket(AddressFamily.InterNetwork,
                               SocketType.Stream, ProtocolType.Tcp);

                IPAddress ipAddress = IPAddress.Parse(Config.HOST_IP);
                //Server is listening on port 1000
                IPEndPoint ipEndPoint = new IPEndPoint(ipAddress, Config.PORT);

                //Connect to the server
                socket.BeginConnect(ipEndPoint,
                    new AsyncCallback(OnConnect), null);
            }
            catch (Exception ex)
            {
            }
        }

        public void OnConnect(IAsyncResult ar)
        {
            //socket = (Socket)ar.AsyncState;
            try
            {
                socket.EndConnect(ar);
                stream = new NetworkStream(socket);
                stream.BeginRead(inMessage.Buffer, 0, 4, new AsyncCallback(ReceiveData), null);

                NetworkMessage message = new NetworkMessage();
                message.AddByte((byte)MessageType.LoginPacket);
                Send(message);
            }
            catch (SocketException)
            {
            }
        }

        public void ReceiveData(IAsyncResult ar)
        {
            if (!EndRead(ar))
            {
                return;
            }

            /*if (inMessage.Length == 4)
            {
                //Don't know why, he recieve another message with 4 len
                //Prob. some echo.. (not always happens)
                stream.BeginRead(inMessage.Buffer, 0, 4, new AsyncCallback(ClientReadCallback), null);
                return;
            }*/


            //Parse
            Parse(inMessage);

            stream.BeginRead(inMessage.Buffer, 0, 4, new AsyncCallback(ReceiveData), null);
        }

        private bool Parse(NetworkMessage message)
        {
            while (message.Position < message.Length - 1)
            {
                switch ((MessageType)message.GetByte())
                {
                    case MessageType.AreaDescriptionPacket:
                        ParseAreaDescription(message);
                        break;
                    case MessageType.ThingsDescriptionPacket:
                        ParseThingsDescription(message);
                        break;
                    case MessageType.PlayerWalkPacket:
                        ParsePlayerWalk(message);
                        break;
                    case MessageType.CreaturesDescriptionPacket:
                        ParseCreaturesDescription(message);
                        break;
                    case MessageType.PlayerMovePacket:
                        ParsePlayerMove(message);
                        break;
                    case MessageType.PlayerLoginPacket:
                        ParsePlayerLogin(message);
                        break;
                    case MessageType.UpdateThingPacket:
                        UpdateCreaturePacket.Parse(message);
                        break;
                    case MessageType.SkillTree:
                        ParseSkillTree(message);
                        break;
                    case MessageType.PlayerSkillsPacket:
                        ParsePlayerSkill(message);
                        break;
                    case MessageType.NewSkill:
                        NewSkill(message);
                        break;
                    case MessageType.SetSkillCooldown:
                        uint skillId = message.GetUInt32();
                        Skill skill = Game1.player.Skills.Where(s => s.Id == skillId).FirstOrDefault();
                        if (skill != null)
                        {
                            skill.StartCooldown();
                        }
                        break;
                    case MessageType.Effect:
                        ParseEffect(message);
                        break;
                }
            }
            return true;
        }
        
        private void ParseEffect(NetworkMessage message)
        {
            byte effect = message.GetByte();
            Point pos = message.GetPosition();
            //START EFFECT IN POSITION!!!
            Game1.GetMap().Effects.Add(new Effect(pos, (Enums.EffectType)effect));
        }

        private void NewSkill(NetworkMessage message)
        {
            Game1.player.Skills.Add(message.GetSkill());
            if (Game1.GetInstance().skillHKHUD.Skills[0] == null)
            {
                Game1.GetInstance().skillHKHUD.Skills[0] = Game1.player.Skills[0];
            }
        }

        private void ParsePlayerSkill(NetworkMessage message)
        {
            uint count = message.GetUInt32();
            for (int i = 0; i < count; i++)
            {
                Game1.player.Skills.Add(message.GetSkill());
            }
        }

        private void ParseSkillTree(NetworkMessage message)
        {
            uint count = message.GetUInt32();
            for (int i = 0; i < count; i++)
            {
                Game1.player.SkillTree.Add(message.GetSkillInfo());
            }
            Game1.GetInstance().skilltreeHUD.Create();
        }

        private void ParsePlayerLogin(NetworkMessage message)
        {
            if (Game1.player == null)
            {
                Game1.player = new Player();
                message.GetPlayer(ref Game1.player);
                Game1.player.SetDir(Config.Direction.N);                
            }
            else
            {
                Creature c = new Creature();
                c.GUID = message.GetGuid();
                c.LoadTileset();
                c.spriteId = message.GetUInt16();
                c.position = message.GetPosition();
                c.Name = message.GetString();
                c.SetStat(Enums.Stat.AGI, message.GetUInt16());
                c.SetStat(Enums.Stat.CONS, message.GetUInt16());
                c.SetStat(Enums.Stat.DEX, message.GetUInt16());
                c.SetStat(Enums.Stat.INT, message.GetUInt16());
                c.SetStat(Enums.Stat.STR, message.GetUInt16());
                c.SetStat(Enums.Stat.WIS, message.GetUInt16());
                c.SetDir(Config.Direction.N);
                Game1.GetMap().Creatures.Add(c);
            }
        }

        private void ParsePlayerMove(NetworkMessage message)
        {
            Guid guid = message.GetGuid();
            Point pos = message.GetPosition();

            Creature c = Game1.GetMap().GetCreatureByGUID(guid);
            if (c != null)
            {
                c.Move(pos);
            }
        }

        private void ParsePlayerWalk(NetworkMessage message)
        {
            Game1.GetPlayer().Move(message.GetPosition());
        }

        private void ParseCreaturesDescription(NetworkMessage message)
        {
            int count = (int)message.GetUInt32();
            for (int i = 0; i < count; i++)
            {
                Creature creature = new Creature();
                creature.GUID = message.GetGuid();
                creature.spriteId = message.GetUInt16();
                creature.position = message.GetPosition();
                creature.Name = message.GetString();
                creature.SetStat(Enums.Stat.AGI, message.GetUInt16());
                creature.SetStat(Enums.Stat.CONS, message.GetUInt16());
                creature.SetStat(Enums.Stat.DEX, message.GetUInt16());
                creature.SetStat(Enums.Stat.INT, message.GetUInt16());
                creature.SetStat(Enums.Stat.STR, message.GetUInt16());
                creature.SetStat(Enums.Stat.WIS, message.GetUInt16());
                creature.LoadTileset();
                creature.SetDir(Config.Direction.N);
                Game1.GetMap().Creatures.Add(creature);
            }
        }

        private void ParseThingsDescription(NetworkMessage message)
        {
            int count = (int)message.GetUInt32();
            for (int i = 0; i < count; i++)
            {
                Thing thing = new Thing();
                thing.GUID = message.GetGuid();
                thing.spriteId = message.GetUInt16();
                thing.position = message.GetPosition();
                Game1.GetMap().Things.Add(thing.position, thing);
            }
        }

        private void ParseAreaDescription(NetworkMessage message)
        {
            Point playerPos = message.GetPosition();
            Game1.GetPlayer().position = playerPos;
            Game1.GetMap().Width = (int)message.GetUInt32();
            Game1.GetMap().Height = (int)message.GetUInt32();
            for (int y = 0; y <= Game1.GetMap().Height; y++)
            {
                for (int x = 0; x <= Game1.GetMap().Width; x++)
                {
                    int ground = message.GetUInt16();
                    if (ground > 0)
                    {
                        Tile tile = new Tile();
                        tile.Ground = ground;
                        Game1.GetMap().SetTile(tile, x, y);
                    }
                }
            }
        }


        private bool EndRead(IAsyncResult ar)
        {
            try
            {
                int read = stream.EndRead(ar);

                if (read == 0)
                {
                    // client disconnected
                    return false;
                }

                int size = (int)BitConverter.ToUInt32(inMessage.Buffer, 0) + 4;

                while (read < size)
                {
                    if (stream.CanRead)
                        read += stream.Read(inMessage.Buffer, read, size - read);
                }
                inMessage.Length = size;

                inMessage.Position = 0;

                inMessage.GetUInt32(); // total length

                return true;
            }
            catch (Exception ex)
            {
                //client disconnected
                return false;
            }
        }


        public void Send(NetworkMessage message)
        {
            message.PrepareToSend();
            try
            {
                stream.Write(message.Buffer, 0, message.Length);
            }
            catch (Exception ex)
            {
                // disconnected
            }
        }
    }
}
