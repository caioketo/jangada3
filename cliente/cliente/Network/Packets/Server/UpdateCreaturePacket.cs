﻿using cliente.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Network.Packets.Server
{
    public class UpdateCreaturePacket
    {
        public static void Parse(NetworkMessage message)
        {
            Guid guid = message.GetGuid();
            Creature creature = Game1.GetMap().GetCreatureByGUID(guid);
            if (creature == null)
            {
                creature = Game1.GetPlayer();
                if (creature == null)
                {
                    return;
                }
            }
            int count = message.GetUInt16();
            for (int i = 0; i < count; i++)
            {
                byte propB = message.GetByte();
                Enums.ThingProp prop = (Enums.ThingProp)propB;
                switch (prop)
                {
                    case Enums.ThingProp.Direction:
                        creature.SetDir((Config.Direction)message.GetUInt16());
                        break;
                    case Enums.ThingProp.Health:
                        creature.Health = (int)message.GetUInt32();
                        break;
                    case Enums.ThingProp.Mana:
                        creature.Mana = (int)message.GetUInt32();
                        break;
                    case Enums.ThingProp.StatsAGI:
                        creature.SetStat(Enums.Stat.AGI, (int)message.GetUInt32());
                        break;
                    case Enums.ThingProp.StatsCONS:
                        creature.SetStat(Enums.Stat.CONS, (int)message.GetUInt32());
                        break;
                    case Enums.ThingProp.StatsDEX:
                        creature.SetStat(Enums.Stat.DEX, (int)message.GetUInt32());
                        break;
                    case Enums.ThingProp.StatsINT:
                        creature.SetStat(Enums.Stat.INT, (int)message.GetUInt32());
                        break;
                    case Enums.ThingProp.StatsSTR:
                        creature.SetStat(Enums.Stat.STR, (int)message.GetUInt32());
                        break;
                    case Enums.ThingProp.StatsWIS:
                        creature.SetStat(Enums.Stat.WIS, (int)message.GetUInt32());
                        break;
                    case Enums.ThingProp.SkillPoints:
                        ((Player)creature).SkillPoints = message.GetUInt32();
                        break;
                    default:
                        break;
                }
            }
        }
    }
}
