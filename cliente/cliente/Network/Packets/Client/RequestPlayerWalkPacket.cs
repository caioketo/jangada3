﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Network.Packets.Client
{
    class RequestPlayerWalkPacket
    {
        public static void Add(NetworkMessage message, cliente.Config.Direction dir)
        {
            message.AddByte((byte)MessageType.RequestPlayerWalkPacket);
            message.AddByte((byte)dir);
        }
    }
}
