﻿using cliente.Content;
using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Network.Packets.Client
{
    public class UseSkillPacket
    {
        public static void Add(NetworkMessage message, Skill skill)
        {
            Add(message, skill, null);
        }
        public static void Add(NetworkMessage message, Skill skill, Nullable<Point> position)
        {
            message.AddByte((byte)MessageType.UseSkill);
            message.AddUInt32((uint)skill.Id);
            if (position.HasValue)
            {
                message.AddByte(0x01);
                message.AddPosition(position.Value);
            }
            else
            {
                message.AddByte(0x00);
            }
        }
    }
}
