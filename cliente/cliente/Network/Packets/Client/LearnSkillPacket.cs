﻿using cliente.Content.Info;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Network.Packets.Client
{
    public class LearnSkillPacket
    {
        public static void Add(NetworkMessage message, SkillInfo skill)
        {
            message.AddByte((byte)MessageType.LearnSkill);
            message.AddUInt16((ushort)skill.Id);
        }
    }
}
