﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Network.Packets.Client
{
    public class RequestSetTargetPacket
    {
        public static void Add(NetworkMessage message, Creature target)
        {
            message.AddByte((byte)MessageType.RequestSetTargetPacket);
            message.AddGuid(target.GUID);
        }
    }
}
