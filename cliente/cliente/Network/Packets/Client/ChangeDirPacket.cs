﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Network.Packets.Client
{
    public class ChangeDirPacket
    {
        public static void Add(NetworkMessage message, cliente.Config.Direction dir)
        {
            message.AddByte((byte)MessageType.ChangeDirPacket);
            message.AddByte((byte)dir);
        }
    }
}
