﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Network
{
    public enum MessageType : byte
    {
        LoginPacket = 0x01,
        AreaDescriptionPacket = 0x02,
        ThingsDescriptionPacket = 0x03,
        RequestPlayerWalkPacket = 0x04,
        PlayerWalkPacket = 0x05,
        CreaturesDescriptionPacket = 0x06,
        PlayerMovePacket = 0x07,
        PlayerLoginPacket = 0x08,
        UpdateThingPacket = 0x09,
        ChangeDirPacket = 0x10,
        RequestSetTargetPacket = 0x11,
        SkillTree = 0x12,
        PlayerSkillsPacket = 0x13,
        LearnSkill = 0x14,
        NewSkill = 0x15,
        UseSkill = 0x16,
        SetSkillCooldown = 0x17,
        Effect = 0x18
    }
}
