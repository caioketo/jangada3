﻿using cliente.Content;
using cliente.Network;
using cliente.Network.Packets.Client;
using cliente.Util;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente
{
    public class MapEngine
    {
        public static bool IsInPolygon(Point[] poly, Point p)
        {
            Point p1, p2;
            bool inside = false;
            if (poly.Length < 3)
            {
                return inside;
            }
            var oldPoint = new Point(
                poly[poly.Length - 1].X, poly[poly.Length - 1].Y);
            for (int i = 0; i < poly.Length; i++)
            {
                var newPoint = new Point(poly[i].X, poly[i].Y);
                if (newPoint.X > oldPoint.X)
                {
                    p1 = oldPoint;

                    p2 = newPoint;
                }
                else
                {
                    p1 = newPoint;

                    p2 = oldPoint;
                }

                if ((newPoint.X < p.X) == (p.X <= oldPoint.X)
                    && (p.Y - (long)p1.Y) * (p2.X - p1.X)
                    < (p2.Y - (long)p1.Y) * (p.X - p1.X))
                {
                    inside = !inside;
                }
                oldPoint = newPoint;
            }
            return inside;
        }

        public Point[] Nquad = new Point[] { new Point(Config.SCREEN_WIDTH / 4, 0), 
            new Point(3 * (Config.SCREEN_WIDTH / 4), 0), new Point(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2) };
        public Point[] NEquad = new Point[] { new Point(3 * (Config.SCREEN_WIDTH / 4), 0), 
            new Point(Config.SCREEN_WIDTH - 1, 0), new Point(Config.SCREEN_WIDTH - 1, Config.SCREEN_HEIGHT / 4), 
            new Point(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2) };
        public Point[] Equad = new Point[] { new Point(Config.SCREEN_WIDTH - 1, Config.SCREEN_HEIGHT / 4), 
            new Point(Config.SCREEN_WIDTH - 1, (Config.SCREEN_HEIGHT / 4) + (Config.SCREEN_HEIGHT / 2)), 
            new Point(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2) };
        public Point[] SEquad = new Point[] { new Point(Config.SCREEN_WIDTH - 1, (Config.SCREEN_HEIGHT / 4) + (Config.SCREEN_HEIGHT / 2)), 
            new Point(Config.SCREEN_WIDTH - 1, Config.SCREEN_HEIGHT - 1), 
            new Point( 3 * (Config.SCREEN_WIDTH / 4), Config.SCREEN_HEIGHT - 1), 
            new Point(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2) };
        public Point[] Squad = new Point[] { new Point(3 * (Config.SCREEN_WIDTH / 4), Config.SCREEN_HEIGHT - 1), 
            new Point(Config.SCREEN_WIDTH / 4, Config.SCREEN_HEIGHT - 1), 
            new Point(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2) };
        public Point[] SWquad = new Point[] { new Point(Config.SCREEN_WIDTH / 4, Config.SCREEN_HEIGHT - 1), 
            new Point(0, Config.SCREEN_HEIGHT - 1), new Point(0, 3 * (Config.SCREEN_HEIGHT / 4)), 
            new Point(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2) };
        public Point[] Wquad = new Point[] { new Point(0, 3 * (Config.SCREEN_HEIGHT / 4)), 
            new Point(0, Config.SCREEN_HEIGHT / 4), 
            new Point(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2) };
        public Point[] NWquad = new Point[] { new Point(0, Config.SCREEN_HEIGHT / 4), new Point(0, 0), 
            new Point(Config.SCREEN_WIDTH / 4, 0), 
            new Point(Config.SCREEN_WIDTH / 2, Config.SCREEN_HEIGHT / 2) };

        public Point[] tileQuad = new Point[] { new Point(32, 0), new Point(64, 16), new Point(32, 32), new Point(0, 16) };

        private TileSet tilesetGround;
        private TileSet tilesetObjects;
        private TileSet tilesetMouse;
        public AoE ActiveArea = null;
        public Skill ActiveAreaSkill = null;
        private Point mousePos = new Point(0, 0);
        public int[] hittest;


        public MapEngine(TileSet _tileset, TileSet _tilesetObjects, TileSet _tilesetMouse)
        {
            tilesetGround = _tileset;
            tilesetObjects = _tilesetObjects;
            tilesetMouse = _tilesetMouse;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (Game1.GetPlayer() == null)
            {
                return;
            }
            int iniY = Game1.GetPlayer().position.Y - 24;
            for (int y = -5;  y <= 45; y++)
            {
                int iniX = Game1.GetPlayer().position.X + 12;
                for (int x = 17; x >= -5; x--)
                {
                    Point screenPos = ConvertCoord(x, y);
                    
                    Tile tileToRender = Game1.GetMap().GetTile(new Point(iniX, iniY));
                    if (iniX == 2 && iniY == 7)
                    {
                        iniX = 2;
                    }
                    if (tileToRender != null && tileToRender.Ground > 0)
                    {
                        tileToRender.Rect = new Rectangle((int)screenPos.X, (int)screenPos.Y + 32,
                            Config.TILE_SIZE / 2, Config.TILE_SIZE / 4);
                        spriteBatch.Draw(tilesetGround.GetTexture(), new Rectangle((int)screenPos.X, (int)screenPos.Y,
                            Config.TILE_SIZE / 2, Config.TILE_SIZE / 2), tilesetGround.TileByIndex(tileToRender.Ground),
                            Color.White);

                        spriteBatch.DrawString(Game1.GetInstance().Font, iniX.ToString() + ", " + iniY.ToString(), new Vector2(screenPos.X + 15, screenPos.Y + 32), Color.White);
                    }
                    iniX--;
                }
                iniY++;
            }

            DrawObjetos(spriteBatch);
            DrawAoE(spriteBatch);
            DrawEffects(spriteBatch);
            Game1.GetInstance().skilltreeHUD.Draw(spriteBatch);
            Game1.GetInstance().skillHKHUD.Draw(spriteBatch);
        }

        protected void DrawEffects(SpriteBatch spriteBatch)
        {
            //foreach (cliente.Content.Effect effect in Game1.GetMap().Effects)
            for (int i = 0; i < Game1.GetMap().Effects.Count; i++)
            {
                cliente.Content.Effect effect = Game1.GetMap().Effects[i];
                if (effect == null || !effect.Enabled)
                {
                    continue;
                }
                if (!effect.sprite.Animating)
                {
                    effect.sprite.Start();
                }
                int x = (5 - Game1.player.position.X) + effect.Position.X;
                int y = (19 - Game1.player.position.Y) + effect.Position.Y;
                Point screenPos = ConvertCoord(x, y);
                effect.Draw(spriteBatch, screenPos);
            }
        }

        protected void DrawAoE(SpriteBatch spriteBatch)
        {
            if (ActiveArea != null)
            {
                ActiveArea.Calculate();

                foreach (Point pos in ActiveArea.Area)
                {
                    int x = (5 - Game1.player.position.X) + pos.X;
                    int y = (19 - Game1.player.position.Y) + pos.Y;
                    Point screenPos = ConvertCoord(x, y);

                    spriteBatch.Draw(tilesetMouse.GetTexture(), new Rectangle((int)screenPos.X, (int)screenPos.Y,
                        Config.TILE_SIZE / 2, Config.TILE_SIZE / 2), tilesetMouse.TileByIndex(1),
                        Color.White);
                }
            }
        }

        protected void DrawObjetos(SpriteBatch spriteBatch)
        {
            Player player = Game1.GetPlayer();
            int iniY = player.position.Y - 24;
            for (int y = -5; y <= 45; y++)
            {
                int iniX = Game1.GetPlayer().position.X + 12;
                for (int x = 17; x >= -5; x--)
                {
                    Thing thing = Game1.GetMap().GetThing(new Point(iniX, iniY));
                    if (thing != null)
                    {
                        Point screenPos = ConvertCoord(x, y);
                        thing.Draw(spriteBatch, screenPos);
                    }
                    Creature creature = Game1.GetMap().GetCreature(new Point(iniX, iniY));
                    if (creature != null)
                    {
                        Point screenPos = ConvertCoord(x, y);
                        creature.Draw(spriteBatch, screenPos);
                    }
                    if (x == 5 && y == 19)
                    {
                        Game1.GetPlayer().Draw(spriteBatch);
                    }
                    iniX--;
                }
                iniY++;
            }
        }

        protected Point ConvertCoord(int mapX, int mapY, bool checkPlayer = true)
        {
            if (checkPlayer && Game1.player.position.Y % 2 == 0 && mapY % 2 == 0)
            {
                mapX++;
            }
            int screenX = mapX * (Config.TILE_SIZE / 2) + ((mapY % 2 == 0 ? 0 : 1) * (Config.TILE_SIZE / 4));
            int screenY = mapY * (Config.TILE_SIZE / 8);
            return new Point(screenX + Game1.player.offsetX, screenY + Game1.player.offsetY);
        }

        public bool KeyPressed(KeyboardState oldKs, KeyboardState ks, Keys key)
        {
            if (ks.IsKeyDown(key))
            {
                if (!oldKs.IsKeyDown(key))
                {
                    return true;
                }
            }
            return false;
        }

        public Point MouseToMap(Point mouse)
        {
            if (Game1.player == null)
            {
                return new Point(0, 0);
            }

            int iniY = Game1.GetPlayer().position.Y - 24;
            for (int y = -5; y <= 45; y++)
            {
                int iniX = Game1.GetPlayer().position.X + 12;
                for (int x = 17; x >= -5; x--)
                {
                    Point screenPos = ConvertCoord(x, y);
                    Tile tileToRender = Game1.GetMap().GetTile(new Point(iniX, iniY));
                    if (tileToRender != null && tileToRender.Rect.Contains(mouse))
                    {
                        int insideX = mouse.X - tileToRender.Rect.X;
                        int insideY = mouse.Y - tileToRender.Rect.Y;
                        if (IsInPolygon(tileQuad, new Point(insideX, insideY)))
                        {
                            return new Point(iniX, iniY);
                        }
                    }
                    iniX--;
                }
                iniY++;
            }
            return new Point(0, 0);
        }

        public void HandleKeyboard(KeyboardState oldKs, KeyboardState ks)
        {
            if (KeyPressed(oldKs, ks, Keys.Tab))
            {
                Creature target = Game1.GetMap().GetClosestTarget();
                if (target != null)
                {
                    Game1.player.SetTarget(target);
                }
            }
            else if (KeyPressed(oldKs, ks, Keys.S))
            {
                Game1.GetInstance().skilltreeHUD.Visible = !Game1.GetInstance().skilltreeHUD.Visible;
            }
        }

        public void UpdateMouse(Point mousePos)
        {
            this.mousePos = MouseToMap(mousePos);
            if (ActiveArea != null)
            {
                ActiveArea.Center = this.mousePos;
            }
            if (Game1.GetPlayer() == null)
            {
                return;
            }
            if (IsInPolygon(Nquad, mousePos))
            {
                Game1.GetPlayer().SetDir(Config.Direction.N);
            }
            else if (IsInPolygon(NEquad, mousePos))
            {
                Game1.GetPlayer().SetDir(Config.Direction.NE);
            }
            else if (IsInPolygon(Equad, mousePos))
            {
                Game1.GetPlayer().SetDir(Config.Direction.E);
            }
            else if (IsInPolygon(SEquad, mousePos))
            {
                Game1.GetPlayer().SetDir(Config.Direction.SE);
            }
            else if (IsInPolygon(Squad, mousePos))
            {
                Game1.GetPlayer().SetDir(Config.Direction.S);
            }
            else if (IsInPolygon(SWquad, mousePos))
            {
                Game1.GetPlayer().SetDir(Config.Direction.SW);
            }
            else if (IsInPolygon(Wquad, mousePos))
            {
                Game1.GetPlayer().SetDir(Config.Direction.W);
            }
            else if (IsInPolygon(NWquad, mousePos))
            {
                Game1.GetPlayer().SetDir(Config.Direction.NW);
            }
            NetworkMessage outMessage = new NetworkMessage();
            ChangeDirPacket.Add(outMessage, Game1.GetPlayer().GetDir());
            Game1.GetConnection().Send(outMessage);
        }

        internal void HandleClick(int p1, int p2)
        {
            /*
            foreach (Thing thing in Game1.GetMap().Things.Values)
            {
                if (thing.Rect.Contains(new Point(p1, p2)))
                {
                    //click thing
                }
            }
            */
            if (Game1.GetInstance().skilltreeHUD.Visible && Game1.GetInstance().skilltreeHUD.SkillTreeRect.Contains(new Point(p1, p2)))
            {
                Game1.GetInstance().skilltreeHUD.HandleClick(p1, p2);
                return;
            }
            if (Game1.GetInstance().skillHKHUD.DrawRect.Contains(new Point(p1, p2)))
            {
                Game1.GetInstance().skillHKHUD.HandleClick(new Point(p1, p2));
                return;
            }
            if (ActiveArea != null)
            {
                NetworkMessage message = new NetworkMessage();
                UseSkillPacket.Add(message, ActiveAreaSkill, ActiveArea.Center);
                Game1.GetConnection().Send(message);
                ActiveArea = null;
                ActiveAreaSkill = null;
                return;
            }

            foreach (Creature creature in Game1.GetMap().Creatures)
            {
                if (creature.Rect.Contains(new Point(p1, p2)))
                {
                    //click creature
                    Game1.player.SetTarget(creature);
                    return;
                }
            }

            if (!Game1.player.IsWalking)
            {
                Game1.player.IsWalking = true;
                NetworkMessage outMessage = new NetworkMessage();
                RequestPlayerWalkPacket.Add(outMessage, Game1.GetPlayer().GetDir());
                Game1.GetConnection().Send(outMessage);
            }
        }
    }
}
