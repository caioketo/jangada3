using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using cliente.Network;
using cliente.Network.Packets.Client;
using cliente.Util;
using System.Timers;
using cliente.Engine;

namespace cliente
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        static Game1 instance;
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        MouseState previousMouseState;
        KeyboardState oldState;
        public MapEngine mapEngine;
        private Dictionary<int, TileSet> creatureTilesets;
        public TileSet creaturesTileSet;
        public TileSet thingsTileset;
        public TileSet skillsTileset;
        public TileSet cooldownTileset;
        static Map map;
        public static Player player;
        static Connection connection;
        public PlayerHUD playerHUD;
        public TargetHUD targetHUD;
        public SkillTreeHUD skilltreeHUD;
        public SkillsHKHUD skillHKHUD;
        public Texture2D line;
        public SpriteFont Font;



        public TileSet GetTileSetCreature(int spriteId)
        {
            TileSet t = null;
            creatureTilesets.TryGetValue(0, out t);
            return t;
        }

        public static Game1 GetInstance()
        {
            return instance;
        }

        public static Connection GetConnection()
        {
            return connection;
        }

        public static Player GetPlayer()
        {
            return player;
        }

        public static Map GetMap()
        {
            return map;
        }


        public Game1()
        {
            instance = this;
            graphics = new GraphicsDeviceManager(this);
            graphics.IsFullScreen = false;
            graphics.PreferredBackBufferWidth = Config.SCREEN_WIDTH;
            graphics.PreferredBackBufferHeight = Config.SCREEN_HEIGHT;
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            base.Initialize();

            previousMouseState = Mouse.GetState();
            IsMouseVisible = true;
            Config.Load();
            map = new Map();
            connection = new Connection();
            connection.StartConnect();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            creaturesTileSet = new TileSet(Content.Load<Texture2D>("char"), 64, 128);
            thingsTileset = new TileSet(Content.Load<Texture2D>("wall"), 64, 128);
            creatureTilesets = TextureContent.LoadListTileSet(Content, "Text");
            mapEngine = new MapEngine(new TileSet(Content.Load<Texture2D>("isometric")),
                new TileSet(Content.Load<Texture2D>("wall"), 64, 128),
                new TileSet(Content.Load<Texture2D>("mouse")));
            Texture2D dummyTexture = new Texture2D(GraphicsDevice, 1, 1);
            dummyTexture.SetData(new Color[] { Color.Red });
            Texture2D dummyTexture2 = new Texture2D(GraphicsDevice, 1, 1);
            dummyTexture2.SetData(new Color[] { Color.Blue });
            playerHUD = new PlayerHUD(Content.Load<Texture2D>("charHUD"), dummyTexture, dummyTexture2);
            targetHUD = new TargetHUD(Content.Load<Texture2D>("targetHUD"), dummyTexture, dummyTexture2);
            cooldownTileset = new TileSet(Content.Load<Texture2D>("cooldown"), 100, 100);
            skillsTileset = new TileSet(Content.Load<Texture2D>("skills"), 100, 100);
            skillHKHUD = new SkillsHKHUD(Content.Load<Texture2D>("skillsHUD"));
            skilltreeHUD = new SkillTreeHUD(Content.Load<Texture2D>("skilltree_bg"), new TileSet(Content.Load<Texture2D>("skillsHelper"), 100, 100),
                new TileSet(Content.Load<Texture2D>("learnBtn"), 60, 30));
            line = new Texture2D(GraphicsDevice, 1, 1, false, SurfaceFormat.Color);
            line.SetData(new[] { Color.White });
            mapEngine.hittest = new int[2048];
            (Content.Load<Texture2D>("testImg")).GetData(mapEngine.hittest);
            Font = Content.Load<SpriteFont>("courier");
            TileSet ts = new TileSet(Content.Load<Texture2D>("explosion"), 25, 25);
            ts.tilesPerLine = 9;
            Config.EffectsTS.Add(ts);
        }

        public void DrawLine(SpriteBatch batch, float width, Color color, Point point1, Point point2)
        {
            Vector2 vec1 = new Vector2(point1.X, point1.Y);
            Vector2 vec2 = new Vector2(point2.X, point2.Y);
            float angle = (float)Math.Atan2(vec2.Y - vec1.Y, vec2.X - vec1.X);
            float length = Vector2.Distance(vec1, vec2);

            batch.Draw(line, vec1, null, color, angle, Vector2.Zero, new Vector2(length, width), SpriteEffects.None, 0);
        }

        protected override void UnloadContent()
        {

        }

        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed ||
                Keyboard.GetState().IsKeyDown(Keys.Escape) && this.IsActive)
                this.Exit();
            MouseState ms = Mouse.GetState();
            if (Mouse.GetState().LeftButton == ButtonState.Pressed
                && this.IsActive
                && ms.X >= 0 && ms.X < graphics.PreferredBackBufferWidth
                && ms.Y >= 0 && ms.Y < graphics.PreferredBackBufferHeight)
            {
                //do your mouse click response...
                if (previousMouseState != ms)
                {
                    mapEngine.HandleClick(ms.X, ms.Y);
                }
            }
            if (previousMouseState != Mouse.GetState())
            {
                mapEngine.UpdateMouse(new Point(Mouse.GetState().X, Mouse.GetState().Y));
            }

            //save the current mouse state for the next frame
            // the current 
            previousMouseState = Mouse.GetState();
            mapEngine.HandleKeyboard(oldState, Keyboard.GetState());
            UpdateInput();
            base.Update(gameTime);
        }


        private void UpdateInput()
        {
            KeyboardState newState = Keyboard.GetState();

            // Is the SPACE key down?
            if (newState.IsKeyDown(Keys.Space))
            {
                // If not down last update, key has just been pressed.
                if (!oldState.IsKeyDown(Keys.Space))
                {
                    
                }
            }
            else if (oldState.IsKeyDown(Keys.Space))
            {
                // Key was down last update, but not down now, so
                // it has just been released.
            }

            // Update saved state.
            oldState = newState;
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            mapEngine.Draw(spriteBatch);
            spriteBatch.End();

            base.Draw(gameTime);
        }

        

        protected Vector2 ConvertBack(int screenX, int screenY)
        {
            Vector2 map = new Vector2();
            map.X = (screenX / 32 + screenY / 16) / 2;
            map.Y = (screenY / 16 - (screenX / 32)) / 2;
            return map;
        }
    }
}
