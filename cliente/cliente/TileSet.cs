﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente
{
    public class TileSet
    {
        private Texture2D tileset;
        private int width = 128;
        private int height = 128;
        public int tilesPerLine = Config.TILES_PER_LINE;
        public TileSet(Texture2D _tileset)
        {
            tileset = _tileset;
        }

        public TileSet(Texture2D _tileset, int _width, int _height)
        {
            tileset = _tileset;
            width = _width;
            height = _height;
        }

        public Rectangle TileByIndex(int id)
        {
            int tileX = (id - 1) % Config.TILES_PER_LINE;
            int tileY = (id - 1) / Config.TILES_PER_LINE;
            return new Rectangle(tileX * width, tileY * height, width, height);
        }

        public Texture2D GetTexture()
        {
            return tileset;
        }

        public int GetWidth()
        {
            return width;
        }

        public int GetHeight()
        {
            return height;
        }
    }
}
