﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Engine
{
    public class TargetHUD
    {
        private Texture2D text;
        private Texture2D hpText;
        private Texture2D mpText;

        public TargetHUD(Texture2D texture, Texture2D hpTexture, Texture2D mpTexture)
        {
            text = texture;
            hpText = hpTexture;
            mpText = mpTexture;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int playerHealthWidth = 0;
            if (Game1.player.Target.MaxHealth > 0)
            {
                playerHealthWidth = (Game1.player.Target.Health * 137) / Game1.player.Target.MaxHealth;
            }

            int playerManaWidth = 0;
            if (Game1.player.Target.MaxMana > 0)
            {
                playerManaWidth = (Game1.player.Target.Mana * 137) / Game1.player.Target.MaxMana;
            }

            spriteBatch.Draw(text, new Rectangle(150, 10, 100, 25), Color.White);
            spriteBatch.Draw(hpText, new Rectangle(207, 5, playerHealthWidth, 6), Color.White);
            spriteBatch.Draw(mpText, new Rectangle(207, 15, playerManaWidth, 7), Color.White);
        }
    }
}
