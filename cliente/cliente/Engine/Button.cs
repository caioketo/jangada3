﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Engine
{
    public delegate void OnClick();
    public class Button
    {
        static int ENABLED = 1;
        static int DISABLED = 2;

        private TileSet tileSet;
        public Point Pos;
        public Point Size;
        public event OnClick OnClick = null;
        public bool Enabled = true;

        public Button(TileSet _tileSet)
        {
            tileSet = _tileSet;
        }

        public Button(TileSet _tileSet, Point pos, Point size)
        {
            tileSet = _tileSet;
            Pos = pos;
            Size = size;
        }

        public void Click()
        {
            if (Enabled && OnClick != null)
            {
                OnClick();
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(tileSet.GetTexture(), new Rectangle(Pos.X, Pos.Y, Size.X, Size.Y),
                tileSet.TileByIndex(Enabled ? ENABLED : DISABLED), Color.White);
        }

        public bool Contains(Point p)
        {
            return (new Rectangle(Pos.X, Pos.Y, Size.X, Size.Y)).Contains(p);
        }

    }
}
