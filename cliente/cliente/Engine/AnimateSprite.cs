﻿using cliente.Content;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Timers;

namespace cliente.Engine
{
    public delegate void OnAnimationEnd();
    public class AnimateSprite
    {
        public event OnAnimationEnd onEnd = null;
        public int AnimationCount = 1;
        public bool Animating = false;
        public int AnimationIndex = 1;
        public TileSet tileSet;
        public int AnimationTotalTime = 10;
        public int Width;
        public int Height;
        private Timer timer;

        public AnimateSprite()
        {
            timer = new Timer();
            timer.Elapsed += (sender, e) => OnTimedEvent(sender, e, this);
            timer.Interval = AnimationTotalTime;
            timer.Enabled = false;
        }

        public void Draw(SpriteBatch spriteBatch, Point pos)
        {
            spriteBatch.Draw(tileSet.GetTexture(), new Rectangle(pos.X, pos.Y, Width, Height), tileSet.TileByIndex(AnimationIndex), Color.White);
        }

        private static void OnTimedEvent(object source, ElapsedEventArgs e, AnimateSprite thisAS)
        {
            thisAS.AnimationIndex++;
            if (thisAS.AnimationIndex >= thisAS.AnimationCount)
            {
                thisAS.AnimationIndex = 1;
                thisAS.Animating = false;
                if (thisAS.onEnd != null)
                {
                    thisAS.onEnd();
                }
            }
        }

        public void Start()
        {
            AnimationIndex = 1;
            if (timer.Enabled)
            {
                timer.Enabled = false;
            }
            timer.Interval = (double)((double)AnimationTotalTime * 1000) / AnimationCount;
            timer.Enabled = true;
            Animating = true;
        }
        

    }
}
