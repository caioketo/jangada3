﻿using cliente.Content.Info;
using cliente.Network;
using cliente.Network.Packets.Client;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Engine
{
    public class SkillTreeHUD
    {
        static int DISABLE_ID = 1;
        static int SELECTED_ID = 2;
        Dictionary<SkillInfo, Point> tree = new Dictionary<SkillInfo, Point>();
        List<Line> lines = new List<Line>();
        private Texture2D texture;
        private TileSet tsHelpers;
        public Point Pos = new Point(50, 80);
        private SkillInfo Selected = null;
        public Rectangle SkillTreeRect;
        private Button learnBtn;
        public bool Visible = true;

        public SkillTreeHUD(Texture2D text, TileSet tshelpers, TileSet learnTS)
        {
            texture = text;
            tsHelpers = tshelpers;
            SkillTreeRect = new Rectangle(Pos.X, Pos.Y, 200, 300);
            learnBtn = new Button(learnTS, new Point(55, 340), new Point(60, 30));
            learnBtn.OnClick += LearnClick;
            learnBtn.Enabled = false;
            Visible = false;
        }
        public void Create()
        {
            List<SkillInfo> skills = Game1.player.SkillTree.OrderBy(s => s.LevelRequired).ThenBy(s => s.PositionInTree).ToList();
            int x = 0;
            int y = 0;
            foreach (SkillInfo skill in skills)
            {
                x = (skill.PositionInTree * 45) + 5;
                y = ((skill.LevelRequired / 5) * 60) + 5;
                tree.Add(skill, new Point(x, y));
            }
            foreach (SkillInfo skill in skills) 
            {
                CreateLines(skill);
            }
        }

        public void CreateLines(SkillInfo skill)
        {
            if (skill.Childs.Count > 0)
            {
                int x = (skill.PositionInTree * 30) + 25 + Pos.X;
                int y = ((skill.LevelRequired / 5) * 30) + 45 + Pos.Y;
                this.lines.Add(new Line(new Point(x, y), new Point(x, y + 10), 2, Color.Black));
                //Desenha linha até a metade, para se tiver mais de 1 child
                SkillInfo firstChild = null;
                SkillInfo lastChild = null;
                foreach (SkillInfo child in skill.Childs)
                {
                    if (firstChild == null)
                    {
                        firstChild = child;
                    }
                    else
                    {
                        if (firstChild.PositionInTree > child.PositionInTree)
                        {
                            firstChild = child;
                        }
                    }
                    if (lastChild == null)
                    {
                        lastChild = child;
                        if (lastChild.PositionInTree < child.PositionInTree)
                        {
                            lastChild = child;
                        }
                    }

                    var xI = (child.PositionInTree * 30) + 25 + Pos.X;
                    var yI = ((child.LevelRequired / 5) * 30) + 35 + Pos.Y;
                    this.lines.Add(new Line(new Point(xI, y + 10), new Point(xI, yI), 2, Color.Black));
                }

                if (firstChild != lastChild) {
                    var xL = (firstChild.PositionInTree * 30) + 25 + Pos.X;
                    var lastX = (lastChild.PositionInTree * 30) + 25 + Pos.X;
                    var yL = ((firstChild.LevelRequired / 5) * 30) + 35 + Pos.Y;
                    this.lines.Add(new Line(new Point(xL, yL), new Point(lastX, yL), 2, Color.Black));
                }
            }
        }


        public void LearnClick()
        {
            if (Selected != null)
            {
                NetworkMessage message = new NetworkMessage();
                LearnSkillPacket.Add(message, Selected);
                Game1.GetConnection().Send(message);
                Selected = null;
            }
        }

        public void HandleClick(int x, int y)
        {
            if (this.learnBtn.Contains(new Point(x, y)))
            {
                this.learnBtn.Click();
                return;
            }

            foreach (SkillInfo skill in tree.Keys)
            {
                Rectangle rect = new Rectangle(tree[skill].X + Pos.X, tree[skill].Y + Pos.Y, 40, 40);
                if (rect.Contains(new Point(x, y)))
                {
                    if (skill.CanLearn)
                    {
                        if (Selected == null || Selected != skill)
                        {
                            Selected = skill;
                            learnBtn.Enabled = true;
                        }
                    }
                    return;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (!Visible)
            {
                return;
            }
            spriteBatch.Draw(texture, new Rectangle(Pos.X, Pos.Y, 200, 300), Color.White);
            foreach (SkillInfo skill in tree.Keys)
            {
                Point skillPos = tree[skill];
                spriteBatch.Draw(Game1.GetInstance().skillsTileset.GetTexture(), new Rectangle(skillPos.X + Pos.X, skillPos.Y + Pos.Y,
                    40, 40), Game1.GetInstance().skillsTileset.TileByIndex(skill.Id), Color.White);
                if (!skill.IsLearnt)
                {
                    spriteBatch.Draw(tsHelpers.GetTexture(), new Rectangle(skillPos.X + Pos.X, skillPos.Y + Pos.Y, 
                        40, 40), tsHelpers.TileByIndex(DISABLE_ID), Color.White);
                }

                if (Selected != null && Selected.Equals(skill))
                {
                    spriteBatch.Draw(tsHelpers.GetTexture(), new Rectangle(skillPos.X + Pos.X, skillPos.Y + Pos.Y,
                        40, 40), tsHelpers.TileByIndex(SELECTED_ID), Color.White);
                }
            }
            foreach (Line line in lines)
            {
                line.Draw(spriteBatch);
            }
            learnBtn.Draw(spriteBatch);
        }
    }
}
