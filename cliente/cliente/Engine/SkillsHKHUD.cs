﻿using cliente.Content;
using cliente.Network;
using cliente.Network.Packets.Client;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Engine
{
    public class SkillsHKHUD
    {   
        public Point Pos = new Point(50, 520);
        private Texture2D Texture;
        public Skill[] Skills = new Skill[10];
        public Keys[] Hotkeys = new Keys[10];
        public Rectangle DrawRect;


        public SkillsHKHUD(Texture2D text)
        {
            Texture = text;
            DrawRect = new Rectangle(20, 550, 500, 50);
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(Texture, DrawRect, Color.White);
            for (int i = 0; i < 10; i++)
            {
                if (Skills[i] != null)
                {
                    int sX = 22 + (i * 50);
                    Skills[i].Draw(spriteBatch, new Point(sX, 552));
                }
            }
        }

        public bool HandleClick(Point position)
        {
            position.X = position.X - 22;
            position.Y = position.Y - 550;
            int i = position.X / 50;
            if (Skills[i] != null && Skills[i].InCooldown == 0)
            {
                if (Skills[i].Targetable)
                {
                    Game1.GetInstance().mapEngine.ActiveAreaSkill = Skills[i];
                    if (Skills[i].AoE != null)
                    {
                        Game1.GetInstance().mapEngine.ActiveArea = Skills[i].AoE;                        
                    }
                }
                else
                {
                    NetworkMessage message = new NetworkMessage();
                    UseSkillPacket.Add(message, Skills[i]);
                    Game1.GetConnection().Send(message);
                }
            }
            return true;
        }
    }
}
