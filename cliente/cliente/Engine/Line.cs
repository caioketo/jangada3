﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Engine
{
    public class Line
    {
        public Point Point1 { get; set; }
        public Point Point2 { get; set; }
        public int Width { get; set; }
        public Color Color { get; set; }

        public Line() { }

        public Line(Point p1, Point p2, int width, Color color)
        {
            Point1 = p1;
            Point2 = p2;
            Width = width;
            Color = color;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            Game1.GetInstance().DrawLine(spriteBatch, Width, Color, Point1, Point2);
        }
    }
}
