﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Engine
{
    public class PlayerHUD
    {
        private Texture2D text;
        private Texture2D hpText;
        private Texture2D mpText;

        public PlayerHUD(Texture2D texture, Texture2D hpTexture, Texture2D mpTexture)
        {
            text = texture;
            hpText = hpTexture;
            mpText = mpTexture;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            int playerHealthWidth = 0;
            if (Game1.player.MaxHealth > 0)
            {
                playerHealthWidth = (Game1.player.Health * 137) / Game1.player.MaxHealth;
            }

            int playerManaWidth = 0;
            if (Game1.player.MaxMana > 0)
            {
                playerManaWidth = (Game1.player.Mana * 137) / Game1.player.MaxMana;
            }

            spriteBatch.Draw(text, new Rectangle(10, 10, 200, 50), Color.White);
            spriteBatch.Draw(hpText, new Rectangle(67, 19, playerHealthWidth, 12), Color.White);
            spriteBatch.Draw(mpText, new Rectangle(67, 39, playerManaWidth, 12), Color.White);
        }
    }
}
