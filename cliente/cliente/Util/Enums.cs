﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente.Util
{
    public class Enums
    {
        public enum ThingProp : byte
        {
            Direction = 0x00,
            Health = 0x01,
            Mana = 0x02,
            StatsSTR = 0x03,
            StatsCONS = 0x04,
            StatsAGI = 0x05,
            StatsDEX = 0x06,
            StatsINT = 0x07,
            StatsWIS = 0x08,
            SkillPoints = 0x09,
            StatsPoints = 0x10
        }

        public enum Stat : byte
        {
            STR = 0x00,
            CONS = 0x01,
            AGI = 0x02,
            DEX = 0x03,
            INT = 0x04,
            WIS = 0x05
        }

        public enum EffectType : byte
        {
            Login = 1,
            Blood = 2,
            Health = 3
        }
    }
}
