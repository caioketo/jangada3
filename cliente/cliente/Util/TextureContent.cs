﻿using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace cliente.Util
{
    public static class TextureContent
    {
        public static Dictionary<int, TileSet> LoadListTileSet(this ContentManager contentManager, string contentFolder)
        {
            DirectoryInfo dir = new DirectoryInfo(contentManager.RootDirectory + "/" + contentFolder);
            if (!dir.Exists)
                throw new DirectoryNotFoundException();
            Dictionary<int, TileSet> result = new Dictionary<int, TileSet>();

            FileInfo[] files = dir.GetFiles("*.*");
            foreach (FileInfo file in files)
            {
                int key = Int32.Parse(file.Name.Substring(0, 5));

                result[key] = new TileSet(contentManager.Load<Texture2D>(contentFolder + "/" +
                    Path.GetFileNameWithoutExtension(file.Name)), 64, 128);
            }
            return result;
        }
    }
}
