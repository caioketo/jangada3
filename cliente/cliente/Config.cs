﻿using cliente.Engine;
using cliente.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace cliente
{
    public class Config
    {
        public static List<TileSet> EffectsTS = new List<TileSet>();
        public static int SCREEN_WIDTH = 800;
        public static int SCREEN_HEIGHT = 600;
        public static int SCREEN_WIDTH_HALF = SCREEN_WIDTH / 2;
        public static int SCREEN_HEIGHT_HALF = SCREEN_HEIGHT / 2;
        public static int TILES_PER_LINE = 5;
        public static int TILE_SIZE = 128;
        public static int TILES_WIDTH_SCREEN = 10;
        public static int TILES_HEIGHT_SCREEN = 30;
        public static int PORT = 7777;
        public static string HOST_IP = "127.0.0.1";
        public static int CENTER_POS_X = 5;
        public static int CENTER_POS_Y = 19;


        public enum Direction : int
        {
            NE = 0,
            NW = 1,
            SW = 2,
            SE = 3,
            N = 4,
            S = 5,
            E = 6,
            W = 7
        }

        public static void Load()
        {
            var iniFile = new IniFile(@"C:\Users\Caio\Documents\jangada3\Jangada3\cliente\cliente\bin\x86\Debug\config.ini");

            HOST_IP = iniFile.IniReadValue("Config", "IP");
        }

        internal static AnimateSprite CreateEffectSprite(Enums.EffectType type)
        {
            AnimateSprite sprite = new AnimateSprite();
            switch (type)
            {
                case Enums.EffectType.Login:
                    break;
                case Enums.EffectType.Blood:
                    sprite.AnimationCount = 70;
                    sprite.Width = 64;
                    sprite.Height = 64;
                    sprite.AnimationTotalTime = 2;
                    sprite.tileSet = EffectsTS[0];
                    break;
                case Enums.EffectType.Health:
                    break;
                default:
                    break;
            }
            return sprite;
        }
    }
}
